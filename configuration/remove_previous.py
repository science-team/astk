# -*- coding: utf-8 -*-

import sys
import os
import re
from glob import glob
from distutils import log
from distutils import sysconfig

from configuration.util import execute, less_than_version, get_function, move_tree, \
                           remove_tree, remove_empty_dirs, tick


def remove_1_7(prefix, dest, version):
   d_f = ['ASTK', 'aster_profile.sh'] \
       + [os.path.join('outils', l) for l in ('as_run', 'show', 'get', 'getop', 'astk', 'bsf')]
   move_tree(prefix, d_f, dest, preserve_symlinks=True)


def remove_last(prefix, dest, version):
   remove_1_7(prefix, dest, version)

   python_lib = re.sub(prefix + '/*', '', sysconfig.get_python_lib(prefix=prefix))
   d_f = [os.path.join('bin', f) for f in ('as_run', 'astk', 'bsf', 'mpirun_template', \
                                           'parallel_cp', 'show', 'get', 'showop', 'getop',
                                           'auto_update.cron')] \
       + ['etc/codeaster', 'share/codeaster/asrun', 'share/codeaster/GPL.txt', 'lib/astk'] \
       + [os.path.join(python_lib, 'asrun'), os.path.join(python_lib, 'astk-%s.egg-info' % version)]
   tick()
   move_tree(prefix, d_f, dest, preserve_symlinks=True, same_device=True)
   tick()


def remove_previous_main(prefix, version):
   """Backup previous installation."""
   if not version:
      version = 'unknown'
   destination = os.path.join(prefix, 'share', 'codeaster', 'backup', 'backup_%s' % version)
   if os.path.exists(destination):
      tick()
      remove_tree(destination)
   os.makedirs(destination)
   log.info("backup previous installation into %s", destination)

   functions = (
      ('0.0.0', remove_1_7),
      ('1.8.0', remove_last),
   )
   tick()
   func = get_function(functions, version)
   tick()
   func(prefix, destination, version)
   tick()
   remove_empty_dirs(prefix)
   tick()
   return destination



