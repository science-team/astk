#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_satellite.tcl 3255 2008-04-10 17:13:17Z courtois $

#################################################################
proc reinit_satellite { } {
   # param�tres stock�s pour le passage satellite normal (0) � AGLA (1)
   set astk::ihm(old,sat,0,noeud) "non_AGLA"
   set astk::ihm(old,sat,0,memoire) 512
   set astk::ihm(old,sat,0,temps) 15:00
   set astk::ihm(old,sat,1,noeud) "AGLA"
   set astk::ihm(old,sat,1,memoire) 512
   set astk::ihm(old,sat,1,temps) 15:00
   maj_sat
   adapt_satellite 0
}

#################################################################
proc affiche_satellite { } {
   global MenuNoeud MenuVers
    # frame subtit : memoire, temps, machine, version...
    pack [frame $astk::ihm(satellite).subtit -relief raised -bd 1 ] -pady 3 -padx 3 -fill both
      # memoire
      set lig 0
       label $astk::ihm(satellite).subtit.lb0 -font $astk::ihm(font,lab) -text [ashare::mess ihm 42]
       entry $astk::ihm(satellite).subtit.mem -width 9  -justify right -font $astk::ihm(font,val) -textvariable astk::profil(memoire)
            $astk::ihm(satellite).subtit.mem configure -validate key -vcmd modprof
      grid  $astk::ihm(satellite).subtit.lb0 -row $lig -column 0 -sticky w
      grid  $astk::ihm(satellite).subtit.mem -row $lig -column 1 -sticky ew

      # memoire Aster
      incr lig
      frame $astk::ihm(satellite).subtit.lbmemast
      label $astk::ihm(satellite).subtit.lbmemast.lb1 -font $astk::ihm(font,lab) -text [ashare::mess ihm 371]
      checkbutton $astk::ihm(satellite).subtit.lbmemast.use -offvalue 0 -onvalue 1 -variable astk::profil(use_mem_aster) -command mod_use_mem_aster
      entry $astk::ihm(satellite).subtit.memast -width 9  -justify right -font $astk::ihm(font,val) -textvariable astk::profil(memoire_aster)
            $astk::ihm(satellite).subtit.memast configure -validate key
            # -vcmd modprof
      pack $astk::ihm(satellite).subtit.lbmemast.lb1 $astk::ihm(satellite).subtit.lbmemast.use -side left
      grid  $astk::ihm(satellite).subtit.lbmemast -row $lig -column 0 -sticky e
      grid  $astk::ihm(satellite).subtit.memast -row $lig -column 1 -sticky ew

      # temps
      incr lig
       label $astk::ihm(satellite).subtit.lb2 -font $astk::ihm(font,lab) -text [ashare::mess ihm 43]
       entry $astk::ihm(satellite).subtit.tps -width 9  -justify right -font $astk::ihm(font,val) -textvariable astk::profil(temps)
            $astk::ihm(satellite).subtit.tps configure -validate key -vcmd modprof
      grid  $astk::ihm(satellite).subtit.lb2 -row $lig -column 0 -sticky w
      grid  $astk::ihm(satellite).subtit.tps -row $lig -column 1 -sticky ew

      # machine
      incr lig
       label $astk::ihm(satellite).subtit.lb3 -font $astk::ihm(font,lab) -text [ashare::mess ihm 45]
      frame $astk::ihm(satellite).subtit.machine
      grid  $astk::ihm(satellite).subtit.lb3 -row $lig -column 0 -sticky w
      grid  $astk::ihm(satellite).subtit.machine -row $lig -column 1 -sticky ew
       liste_machines $astk::ihm(satellite).subtit.machine

      # version
      incr lig
       label $astk::ihm(satellite).subtit.lb4 -font $astk::ihm(font,lab) -text [ashare::mess ihm 46]
      frame $astk::ihm(satellite).subtit.version
      grid  $astk::ihm(satellite).subtit.lb4 -row $lig -column 0 -sticky w
      grid  $astk::ihm(satellite).subtit.version -row $lig -column 1 -sticky ew

      # batch / interactif
       radiobutton $astk::ihm(satellite).subtit.batch -font $astk::ihm(font,lab) -text [ashare::mess ihm 51] -value 1 -variable astk::profil(batch) -command mod_batch
       radiobutton $astk::ihm(satellite).subtit.inter -font $astk::ihm(font,lab) -text [ashare::mess ihm 52] -value 0 -variable astk::profil(batch) -command mod_batch
      incr lig
       grid $astk::ihm(satellite).subtit.batch -row $lig -column 0 -columnspan 2 -padx 15 -sticky sw
      incr lig
      grid $astk::ihm(satellite).subtit.inter -row $lig -column 0 -columnspan 2 -padx 15 -sticky w

      # suivi
      incr lig
       checkbutton $astk::ihm(satellite).subtit.choix -offvalue 0 -onvalue 1 -variable astk::profil(suivi_interactif) -text [ashare::mess ihm 47] -command mod_suivi
      grid $astk::ihm(satellite).subtit.choix -row $lig -column 0 -columnspan 2

      # pas r�ussi a faire autrement que mettre une ligne vide !
      incr lig
      label $astk::ihm(satellite).subtit.lv0 -font $astk::ihm(font,lab) -text " "
      grid $astk::ihm(satellite).subtit.lv0 -row $lig -column 0
    # frame debug / nodebug
       radiobutton $astk::ihm(satellite).subtit.nodebug -font $astk::ihm(font,lab) -text nodebug -value 0 -variable astk::profil(debug)
       radiobutton $astk::ihm(satellite).subtit.debug -font $astk::ihm(font,lab) -text debug -value 1 -variable astk::profil(debug)
      incr lig
      grid $astk::ihm(satellite).subtit.nodebug -row $lig -column 0 -columnspan 2 -padx 15 -sticky sw
      incr lig
      grid $astk::ihm(satellite).subtit.debug -row $lig -column 0 -columnspan 2 -padx 15 -sticky nw

    # frame bas : lancer / suivi des jobs
    pack [frame $astk::ihm(satellite).bas -relief raised -bd 1] -pady 3 -ipady 20 -padx 3 -fill x -expand 1 -anchor n
# bouton Lancer
    button $astk::ihm(satellite).bas.go -font $astk::ihm(font,labbout) -text [ashare::mess ihm 189] \
      -command { change_status "[ashare::mess ihm 49]..." ; go_calcul } \
      -background $astk::ihm(couleur,lancement) -activebackground $astk::ihm(couleur,valid)
# Menu mode de lancement : RUN (make_etude=oui), DBG (make_etude=dbg), PREP (make_etude=non)
   set lch [list $astk::ihm(mode,run) $astk::ihm(mode,dbg) $astk::ihm(mode,pre)]
   set i 0
   set MenuMode [tk_optionMenu $astk::ihm(satellite).bas.mode astk::profil(make_etude) [lindex $lch $i]]
   $MenuMode configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuMode entryconfigure 1 -font $astk::ihm(font,labbout)
   for {set i 1} {$i < [llength $lch]} {incr i} {
      $MenuMode add radiobutton
      $MenuMode entryconfigure $i -label [lindex $lch $i] -font $astk::ihm(font,labbout) \
         -variable astk::profil(make_etude) -command {chg_lancement}
   }
   $astk::ihm(satellite).bas.mode configure -relief raised -font $astk::ihm(font,labbout) \
      -background $astk::ihm(couleur,mode_lancement) -activebackground $astk::ihm(couleur,mode_lancement)

# bouton suivi des jobs
    button $astk::ihm(satellite).bas.bjobs -font $astk::ihm(font,labbout) -text [ashare::mess ihm 48] \
        -background $astk::ihm(couleur,suivi) -activebackground $astk::ihm(couleur,suivi) \
        -command "show_fen $astk::ihm(asjob) force"

    grid $astk::ihm(satellite).bas.go    -row 0 -column 0 -sticky nsew -padx 5 -pady 5
   grid $astk::ihm(satellite).bas.mode  -row 0 -column 1 -sticky nsew -padx 5 -pady 5
    grid $astk::ihm(satellite).bas.bjobs -row 1 -column 0 -columnspan 2 -sticky nsew -padx 5 -pady 5

   reinit_satellite
# la modif des valeurs dans les entry produit un modprof (on)
   modprof off
# aide contextuelle
   def_context $astk::ihm(satellite).subtit.mem 309
   def_context $astk::ihm(satellite).subtit.memast 372
   def_context $astk::ihm(satellite).subtit.tps 310
   def_context $astk::ihm(satellite).subtit.batch 314
   def_context $astk::ihm(satellite).subtit.inter 315
   def_context $astk::ihm(satellite).subtit.choix 316
   def_context $astk::ihm(satellite).subtit.nodebug 317
   def_context $astk::ihm(satellite).subtit.debug 317
   def_context $astk::ihm(satellite).bas.go 318
   def_context $astk::ihm(satellite).bas.mode 319
   set ashare::context($MenuMode,0) 279
   set ashare::context($MenuMode,1) 280
   set ashare::context($MenuMode,2) 103
   bind $MenuMode <<MenuSelect>> {ShowStatus menu %W astk}
   def_context $astk::ihm(satellite).bas.bjobs 320
}

# modif use_mem_aster
#################################################################
proc mod_use_mem_aster { } {
# quand on d�coche, disabled + mettre memoire
   if { $astk::profil(use_mem_aster) == 0 } {
      $astk::ihm(satellite).subtit.memast configure -state disabled
   } else {
      $astk::ihm(satellite).subtit.memast configure -state normal
   }
}

# modif batch
#################################################################
proc mod_batch { } {
   set serv $astk::inv(serv,$astk::profil(serveur))
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (mod_batch) IN  : $astk::profil(batch)"
   }
   # possibilit�s du noeud
   if { $astk::config($serv,batch) == "oui" } {
      $astk::ihm(satellite).subtit.batch configure -state normal
   } else {
      $astk::ihm(satellite).subtit.batch configure -state disabled
   }
   if { $astk::config($serv,interactif) == "oui" } {
      $astk::ihm(satellite).subtit.inter configure -state normal
      $astk::ihm(satellite).subtit.choix configure -state normal
      mod_lancement normal
   } else {
      $astk::ihm(satellite).subtit.inter configure -state disabled
      $astk::ihm(satellite).subtit.choix configure -state disabled
      mod_lancement disabled
   }
   # cons�quence du choix
   if { $astk::profil(batch) } {
      if { $astk::config($serv,batch) == "oui" } {
         set astk::profil(suivi_interactif) 0
         $astk::ihm(satellite).subtit.choix configure -state disabled
         mod_lancement disabled
      } else {
         set astk::profil(batch) 0
      }
   } else {
      if { $astk::config($serv,interactif) == "oui" } {
         $astk::ihm(satellite).subtit.choix configure -state normal
         mod_lancement normal
      } else {
         set astk::profil(batch) 1
      }
   }
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (mod_batch) OUT : $astk::profil(batch)"
   }
}

# modif le mode de lancement
#################################################################
proc mod_lancement { etat {val "run"}} {
   if { $astk::profil(etude) != "oui" } {
      set etat disabled
   }
   set astk::profil(make_etude) $astk::ihm(mode,$val)
   $astk::ihm(satellite).bas.mode configure -state $etat
}

# action quand on modifie le mode de lancement
#################################################################
proc chg_lancement { } {
# si "dbg" ou "pre", on valide "debug" par d�faut
   if { $astk::profil(make_etude) == $astk::ihm(mode,dbg)
     || $astk::profil(make_etude) == $astk::ihm(mode,pre) } {
      set astk::profil(debug) 1
   }
# si "dbg", suivi_interactif obligatoire
   if { $astk::profil(make_etude) == $astk::ihm(mode,dbg) } {
      set astk::profil(suivi_interactif) 1
   }
}

# modif suivi_interactif
#################################################################
proc mod_suivi { } {
# si on d�coche, v�rifier qu'on n'est pas en "dbg"
   if { $astk::profil(suivi_interactif) == 0 } {
      chg_lancement
   }
}

# mise � jour du satellite : liste des noeuds, liste des versions, batch/interactif...
#################################################################
proc maj_sat { } {
   cree_liste_machines
   liste_machines $astk::ihm(satellite).subtit.machine
   if { $astk::ihm(old,sat,0,noeud) == "non_AGLA" } {
      set astk::ihm(old,sat,0,noeud) $astk::ihm(lnoeud,0)
   }
   change_noeud
   mod_use_mem_aster
}

# contenu adapt� � l'onglet
#  0 : satellite normal
#  1 : satellite pour agla
#  2 : satallite pour agla sans changer les param�tres m�moire/temps
#################################################################
proc adapt_satellite { level } {
   global last_level
# si pas de chgt, on sort
   set mod 0
   catch {
      if { $level == $last_level } {
         set mod 1
      } else {
      # conserve les anciennes valeurs
         foreach mcle { noeud memoire temps } {
            set astk::ihm(old,sat,$last_level,$mcle) $astk::profil($mcle)
         }
      }
   }
   if { $mod } { return }

   set last_level $level

   set level_param $level
   # level = 2 : on prend les param�tres m�moire/temps non_AGLA
   if { $level == 2 } {
      set level_param 0
      set level 1
   }
   set astk::profil(noeud) $astk::ihm(old,sat,$level,noeud)

   set astk::profil(memoire) $astk::ihm(old,sat,$level_param,memoire)
   set astk::profil(temps) $astk::ihm(old,sat,$level_param,temps)

   switch $level {
      0 {
         $astk::ihm(satellite).subtit.choix configure -state normal
         $astk::ihm(satellite).subtit.batch configure -state normal
         $astk::ihm(satellite).subtit.inter configure -state normal
         mod_lancement normal
         $astk::ihm(satellite).subtit.nodebug configure -state normal
         $astk::ihm(satellite).subtit.debug configure -state normal
         change_noeud
         $astk::ihm(satellite).subtit.lb3 configure -state normal
         $astk::ihm(satellite).subtit.machine.noeud configure -state normal
      }
      1 {
         set astk::profil(suivi_interactif) 0
         $astk::ihm(satellite).subtit.choix configure -state disabled
         set astk::profil(batch) 1
         $astk::ihm(satellite).subtit.batch configure -state disabled
         $astk::ihm(satellite).subtit.inter configure -state disabled
         mod_lancement disabled
         $astk::ihm(satellite).subtit.nodebug configure -state disabled
         $astk::ihm(satellite).subtit.debug configure -state disabled
         set astk::profil(serveur) $astk::config($astk::agla(num_serv),nom)
         $astk::ihm(satellite).subtit.lb3 configure -state disabled
         $astk::ihm(satellite).subtit.machine.noeud configure -state disabled
         liste_vers_agla $astk::ihm(satellite).subtit.version
          if { $ashare::dbg >= 4 } {
            ashare::log "<DEBUG> (adapt_satellite) Nouveaux serveur/noeud/version : $astk::profil(serveur) $astk::profil(noeud) $astk::profil(version)"
         }
      }
   }
}
