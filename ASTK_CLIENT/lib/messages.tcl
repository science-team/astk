#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: messages.tcl 1023 2005-10-27 08:04:48Z mcourtoi $

# mots-cl�s du fichier de messages
#################################################################
proc ashare::init_mots_msg { } {
# MCS : Mots-Cl�s Simples
# MCF : Mot-Cl� Facteur (un seul)
# SSF : mots-cl�s Simples Sous un mot-cl� Facteur
# fichier des messages
   set ashare::mots(MCS_msg) { nb_err nb_inf nb_ihm }
   set ashare::mots(MCF_msg) { langue }
   set ashare::mots(SSF_msg) { ihm erreur info }
}

# affichage des messages d'erreur et info
#################################################################
proc ashare::mess { typ indice {args ""} } {
   set msg ""
   if { $typ == "erreur" } {
      if { $ashare::fen_log < 2 } { set ashare::fen_log 0 }
      append msg "<$ashare::msg($ashare::lang,$typ,txt)"
      append msg [format " %03d%1s" $indice ">"]
   } elseif { $typ == "info" } {
      append msg "<$ashare::msg($ashare::lang,$typ,txt)>"
   } elseif { $typ == "ihm" } {
      # RAS
   } else {
      set iret "<ERROR 000> Bad message type."
      ashare::log $iret
      return $iret
   }
# construction du format (pour d�calage)
   if { $typ != "ihm" } {
      set fmt "%"
#      append fmt [string length $msg]
      append fmt "s%s"
      append msg " "
   }
# insertion des arguments
   append msg $ashare::msg($ashare::lang,$typ,$indice)
   set narg [llength $args]
   for {set i 1} {$i <= $narg} {incr i} {
      regsub -all "@$i@" $msg [lindex $args [expr $i - 1]] msg
   }
   regsub -all "@\[0-9\]*@" $msg "-" msg
#
   set CR "@n@"
   if { $typ == "ihm" } {
      regsub -all $CR $msg "\n" msg2
   } else {
      set msg2 ""
      set n 0
      set n1 1
      while { $n1 > 0 } {
         set n1 [string first $CR $msg $n]
         set sub [string range $msg $n [expr $n1 - 1]]
         if { $n1 < 0 } {
            set sub [string range $msg $n [string length $msg]]
         }
         if { $n != 0 } {
            set sub [format "$fmt" "" $sub]
         }
         ashare::log $sub
         append msg2 $sub
         append msg2 "\n"
         set n [expr $n1 + [string length $CR]]
      }
   }
   return $msg2
}

# initialisation des messages erreur/info/ihm
#################################################################
proc ashare::lire_msg { fich } {
   set iret [ashare::lire_mc_val $fich mots vale nlu ]
   if { $iret != 0 } {
      return $iret }

   # phase de v�rif
   # mots-cl�s reconnus
   set mots_cles $ashare::mots(MCS_msg)
   append mots_cles $ashare::mots(MCF_msg)
   # mots-cl�s sous le mot-cl� "langue"
   set mots_smcf $ashare::mots(SSF_msg)
   append mots_cles $mots_smcf
   # les mots-cl�s sont obligatoires
   set pmcs [llength $mots_smcf]
   set pmcf [expr [llength $mots_cles] - $pmcs - 1]
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
   }

   set nberr 0
   set lang ""
   set lang_lue 0
   set ashare::llang(nb_lang) 0
   set type ""
   set nb(erreur) 0
   set nb(info) 0
   set nb(ihm) 0
   for {set j 1} {$j <= $nlu} {incr j} {
      #astkrc_version : pas utilis� ici
      if { $mots($j) == "astkrc_version" } {
         if { $vale($j) != "$ashare::astkrc_version" } {
            ashare::log "<ERROR> astkrc version incorrect : $fich"
            return PB_astkrc_version
         }
      } else {
         set iv [lsearch -exact $mots_cles $mots($j)]
         if { $iv < 0 && [regexp {^[ ]*([0-9]+)[ ]*} $mots($j)] == 0 } {
            ashare::log "<ERROR> Unknown keyword : $mots($j)"
            incr nberr
         } else {
            set i [lsearch -exact $mots_smcf $mots($j)]
            if { $i < 0 && [regexp {^[ ]*([0-9]+)[ ]*} $mots($j)] == 0 } {
               if { $mots($j) == "langue" } {
                  set ashare::llang(lang,$ashare::llang(nb_lang)) $vale($j)
                  incr ashare::llang(nb_lang)
                  set lang $vale($j)
                  set lang_lue 1
               } else {
                  set ashare::msg($mots($j)) $vale($j)
               }
            } elseif { $i >= 0 } {
               if { [string length $lang] > 0 } {
                  set ashare::msg($lang,$mots($j),txt) $vale($j)
                  set typ $mots($j)
               }
            } else {
               if { [string length $lang] > 0 && [string length $typ] > 0 } {
                  set ashare::msg($lang,$typ,$mots($j)) $vale($j)
                  incr nb($typ)
               }
            }
         }
      }
   }
   if { [string length $lang] == 0 || ! $lang_lue } {
      ashare::log "<ERROR> Unknown language."
      incr nberr
   } else {
      if { $nb(erreur) != [expr $ashare::msg(nb_err) * $ashare::llang(nb_lang)] } {
         ashare::log "<ERROR> Number of ERROR messages incorrect."
         incr nberr
      }
      if { $nb(info) != [expr $ashare::msg(nb_inf) * $ashare::llang(nb_lang)] } {
         ashare::log "<ERROR> Number of INFO messages incorrect."
         incr nberr
      }
      if { $nb(ihm) != [expr $ashare::msg(nb_ihm) * $ashare::llang(nb_lang)] } {
         ashare::log "<ERROR> Number of GUI messages incorrect."
         incr nberr
      }
   }
   if { $nberr > 0 } {
      ashare::log "<ERROR> Read of $fich failed."
      ashare::my_exit 4
   }
# debug maxi
   if { $ashare::dbg >= 5 } {
      for {set i 1} {$i <= $ashare::msg(nb_err)} {incr i} {
         ashare::log "ERR $i : $ashare::msg(erreur,$i)" }
      for {set i 1} {$i <= $ashare::msg(nb_inf)} {incr i} {
         ashare::log "INF $i : $ashare::msg(info,$i)" }
      for {set i 1} {$i <= $ashare::msg(nb_ihm)} {incr i} {
         ashare::log "IHM $i : [ashare::mess ihm $i]" }
   } 
   return 0
}

# Affichage des messages dans une fen�tre de log + fen�tre
#################################################################
proc ashare::log { chaine } {
   set fen $astk::ihm(log)
   set wtxt $fen.txt.tx
# si la fenetre des logs n'existe pas, on fait un puts sur stdout
   if { $ashare::fen_log < 1 } {
      puts $chaine
   } else {
      puts $ashare::pt_log $chaine
      flush $ashare::pt_log
      if { [winfo exists $fen] } {
         $fen.txt.tx configure -state normal
         $fen.txt.tx insert end $chaine
         $fen.txt.tx insert end "\n"
         $fen.txt.tx configure -state disabled
         $fen.txt.tx see end
      }
   }
}

# ouverture du fichier des logs
#################################################################
proc ashare::init_log { fic } {
   append ashare::fic_log $fic
   set ashare::pt_log [open $ashare::fic_log w+]
   set start [clock format [clock seconds] -format "%d/%m/%Y - %H:%M:%S"]
   ashare::log "<INFO> Session started : $start (process id [pid])"
}

# fenetre des logs
#   fen_log = 0 : on ne veut pas de la fenetre
#   fen_log = 1 : en cours d'init, message erreur sur stdout
#   fen_log = 2 : init fini
#################################################################
proc ashare::affich_log { } {
   if { $ashare::fen_log == 0 } {
      return
   }
# ouverture du fichier
   set fen $astk::ihm(log)
   catch {destroy $fen}
   toplevel $fen
   set_icon $fen
   # on cache la fen�tre en attendant la construction
   wm withdraw $fen
   wm title $fen "Messages"
   wm protocol $fen WM_DELETE_WINDOW {$astk::ihm(log).valid.ok invoke}
   bind $fen <Control-Key-w> "destroy $fen"
   
#  texte
   pack [frame $fen.txt -relief solid -bd 0] -anchor c -fill both -expand yes
   text $fen.txt.tx -xscrollcommand "$fen.txt.scrollx set" -yscrollcommand "$fen.txt.scrolly set" \
      -height 15 -width 100 -font $astk::ihm(font,txtfix) -bg $astk::ihm(couleur,entry_background) -wrap none
   scrollbar $fen.txt.scrolly -command "$fen.txt.tx yview"
   scrollbar $fen.txt.scrollx -command "$fen.txt.tx xview" -orient h
   pack $fen.txt.scrolly -side right  -fill y
   pack $fen.txt.scrollx -side bottom -fill x
   pack $fen.txt.tx -expand 1 -fill both

# remplit le texte
   set vol [file size $ashare::fic_log]
   seek $ashare::pt_log 0 start
   $fen.txt.tx configure -state normal
   $fen.txt.tx insert end [read $ashare::pt_log]
   $fen.txt.tx configure -state disabled
   $fen.txt.tx see end

#  fermer
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "destroy $fen"
   pack $fen.valid.ok -padx 10 -pady 3
   wm deiconify $fen
   ashare::trace_geom log $fen
}

