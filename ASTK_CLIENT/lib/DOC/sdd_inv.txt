Tableaux inverses pour acc�l�rer la recherche de
la correspondance nom - indice de tableau :


astk::inv(serv,"Serveur")                = num tq astk::config(num,nom)="Serveur"

astk::inv(cmpl,"clayastr...","mcourtoi") = num tq
      astk::config(num,nom_complet)="clayastr.cla.edfgdf.fr"
      astk::config(num,login)      ="mcourtoi"

astk::inv(outil,"Outil")                 = num tq astk::outil(num,nom)="Outil"
