Introduction � astk


--------------------
astk (prononcer "ASTeK") est l'interface de lancement de Code_Aster.
Il est �crit en Tcl/Tk, d'o� son nom (Aster + Tk).

Il s'agit d'un gestionnaire d'�tude qui permet de lancer des calculs Aster, �ventuellement avec surcharge du code source original, sur une machine locale ou sur un serveur distant en mode interactif ou batch.
Les d�veloppeurs Aster ont aussi acc�s aux fonctionnalit�s de l'atelier de g�nie logiciel Aster (AGLA) : v�rification du code source, passage de tests de non r�gression, restitution des d�veloppements...


--------------------
Multi-machines ?

Cela signifie que les fichiers utilis�s pour les �tudes peuvent se trouver sur la machine locale ou bien sur un serveur distant.

- La machine appel�e "Local" correspond � la machine depuis laquelle a �t� lanc� astk. Ses propri�t�s sont d�finies parmi les pr�f�rences de l'utilisateur (menu Configuration/Interface).
  Si Code_Aster est install� sur cette machine, il faut ajouter un serveur ayant l'adresse de cette machine.
- Les "serveurs" sont aussi bien des serveurs de fichiers que des serveurs de calcul Aster.
  Il faut ajouter les serveurs sur lesquels vous souhaitez ex�cuter Aster (la partie serveur d'astk doit y �tre install�e, r�pertoire ASTK_SERV), et les serveurs sur lesquels se trouvent vos fichiers.

Les protocoles d'�change support�s entre les serveurs et la machine locale sont :
   - rsh ou ssh (d�faut) pour l'ex�cution de commandes,
   - rcp, scp (d�faut) ou rsync pour la copie de fichiers.

Voir Aide/Menu Fichier/Configuration/Outils pour plus de d�tails sur la configuration des serveurs.


--------------------
Logique de fonctionnement

Les fichiers n�cessaires au lancement des calculs Aster sont fournis sous "l'onglet" ETUDE. A l'aide des icones situ�s sur la droite, on peut ins�rer des lignes dans la liste, ajouter un nouveau fichier, le supprimer ou l'�diter.
Le type correspondant au type Aster (avec son unit� logique associ�e).
   "D" doit �tre coch� si le fichier/r�pertoire est une donn�e,
   "R" s'il s'agit d'un r�sultat.
   "C" permet de pr�ciser que le fichier (ou les fichiers contenus dans le r�pertoire) doit �tre d�/compress� avec gzip (ceci est utile pour les bases ou les fichiers r�sultats volumineux).

Pour r�aliser une surcharge du code, il faut fournir les sources sous "l'onglet" SURCHARGE (fortran, C, catalogues...) dans la partie sup�rieure, et les produits de cette surcharge dans la partie inf�rieure (ex�cutable, catalogues compil�s...).

On peut acc�der � l'onglet TESTS en maintenant le clic sur le bouton ETUDE.
Cet onglet permet de fournir une liste de tests � lancer, en fournissant �ventuellement des tests suppl�mentaires.

Enfin, pour que le contenu d'un onglet soit pris en compte, il faut le cocher !

Pour acc�der au fonctionnalit�s de d�veloppements sur la machine de r�f�rence :
 - il faut �tre enregistr� en tant que d�veloppeur (contacter code-aster@edf.fr),
 - cocher l'onglet AGLA (qui coche automatiquement les onglets SURCHARGE et TESTS).



# $Id: item_110.txt 753 2004-12-06 08:48:48Z mcourtoi $
