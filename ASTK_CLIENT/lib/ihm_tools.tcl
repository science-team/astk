#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_tools.tcl 3649 2008-11-28 16:01:22Z courtois $

# change le pointeur en position "attente"
#################################################################
proc ashare::pointeur { etat } {
# voir X11/cursorfont.h (watch,...)
   set lw [winfo children .]
   set nw [llength $lw]
   for {set i 0} {$i < $nw} {incr i} {
      set w [lindex $lw $i]
      if { $etat == "off" } {
         $w configure -cursor watch
      } else {
         $w configure -cursor {}
      }
   }
   update idletasks
}

# demande une valeur
# parent : fenetre parent
# ntit : num�ro message ihm du titre
# nasw : num�ro message ihm de la question
# valint : liste des valeurs interdites
# vinit : valeur initiale
# ivid : "vide" si on accepte la valeur "",
#        "non_vide", si on refuse la valeur "", dans ce cas, on retourne "_VIDE"
# ibla : 1 on laisse les blancs, 0 on les supprime
#################################################################
proc getValue_fen { parent ntit nasw valint vinit ivid {ibla 0}} {
   global getValue_val
   set fen .fen_getv
   catch {destroy $fen}
   toplevel $fen
   wm title $fen [ashare::mess ihm $ntit]
   wm withdraw $fen
   wm transient $fen $parent
   wm protocol $fen WM_DELETE_WINDOW "set getValue_val $vinit ; grab release .fen_getv ; destroy .fen_getv"
   grab set $fen
   set getValue_val $vinit
   frame $fen.txt -relief flat -bd 0
   pack $fen.txt -fill x -expand 1
   label $fen.txt.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm $nasw] :"
   entry $fen.txt.chp -width 20 -font $astk::ihm(font,val) -textvariable getValue_val
   $fen.txt.chp icursor end
   button $fen.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "verif_valeur $fen {$valint} $ivid $ibla"
   bind $fen <Return> "$fen.ok invoke"

   pack $fen.txt.lbl -padx 10 -pady 5 -side left
   pack $fen.txt.chp -padx 10 -pady 5 -side right
   pack $fen.ok

   # position
   ashare::centre_fen $fen $parent
   wm deiconify $fen
   focus $fen.txt.chp
   update
}

# demande une valeur radiobutton
# parent : fenetre parent
# ntit : num�ro message ihm du titre
# nasw : label
# values : liste des valeurs possibles
# vinit : valeur initiale
#################################################################
proc getValue_chb { parent ntit nasw values vinit } {
   global getValue_val
   set fen .fen_getv
   catch {destroy $fen}
   toplevel $fen
   wm title $fen [ashare::mess ihm $ntit]
   wm withdraw $fen
   wm transient $fen $parent
   wm protocol $fen WM_DELETE_WINDOW "set getValue_val $vinit ; grab release .fen_getv ; destroy .fen_getv"
   grab set $fen
   set getValue_val $vinit
   frame $fen.txt -relief flat -bd 0
   pack $fen.txt -fill x -expand 1
   label $fen.txt.lbl -font $astk::ihm(font,lab) -text "$nasw :"
   radiobutton $fen.txt.yes -font $astk::ihm(font,val) -text [ashare::mess ihm 224] \
        -value [string tolower [ashare::mess ihm 224]] -variable getValue_val
   radiobutton $fen.txt.no -font $astk::ihm(font,val) -text [ashare::mess ihm 225] \
        -value [string tolower [ashare::mess ihm 225]] -variable getValue_val
   pack $fen.txt.lbl $fen.txt.yes $fen.txt.no -padx 10 -pady 5 -side left

   button $fen.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "verif_valeur $fen {} vide 0"
   bind $fen <Return> "$fen.ok invoke"
   pack $fen.ok

   # position
   ashare::centre_fen $fen $parent
   wm deiconify $fen
   update
}

# v�rifie que la valeur est valide
# ivid, ibla : voir getValue_fen
#################################################################
proc verif_valeur { fen valint ivid ibla} {
   global getValue_val
   # pas de blancs
   if { $ibla == 0 } {
      regsub -all " " $getValue_val "" getValue_val
   }
# valeur "" non autoris�e
   if { $getValue_val == "" && $ivid == "non_vide" } {
      set getValue_val "_VIDE"
# parmi les valeurs interdites
   } elseif { [lsearch $valint $getValue_val] > -1 } {
      set msg [ashare::mess info 17 $getValue_val]
      set iret [ tk_messageBox -message $msg -title [ashare::mess ihm 138] -type ok -icon info -parent $fen]
      set getValue_val ""
      return
   }
   grab release $fen ; destroy $fen
}

# centre une fenetre par rapport � son parent
#################################################################
proc ashare::centre_fen { fen parent } {
    update idletasks
   set x0 0
   set y0 0
   set w0 0
   set h0 0
    regexp {([0-9]+)x([0-9]+)\+([0-9]+)\+([0-9]+)} [winfo geometry  $parent] mat1 w0 h0 x0 y0
    set w1 [winfo reqwidth  $fen]
    set h1 [winfo reqheight $fen]
   set x1 [expr $x0+($w0-$w1)/2]
   set y1 [expr $y0+($h0-$h1)/4]
#ashare::log "$w0 x $h0 + $x0 + $y0  >>>  + $x1 + $y1"
   if { $x1 < 0 } { set x1 0 }
   if { $y1 < 0 } { set y1 0 }
   wm geometry $fen "+$x1+$y1"
}

# montre une fen�tre (desiconifie, avant-plan...)
# si besoin, reinitialise la fenetre
# fen=asjob, log
#################################################################
proc show_fen { fen {arg ""}} {
   if { [winfo exists $fen] == 0 } {
      ashare::pointeur off
      if { $fen == $astk::ihm(asjob) } {
         toplevel $fen
         asjob_main $fen
      } elseif { $fen == $astk::ihm(log) } {
         ashare::affich_log
      } elseif { $fen != "." } {
      # pour ".", on ne fait rien
         ashare::mess erreur 11 $fen
      }
      ashare::pointeur on
   } elseif { $arg == "force" } {
      wm deiconify $fen
      raise $fen
   }
}

# tk_messageBox yesno + yesall
# tit = titre de la fenetre
# msg = message
#################################################################
proc ashare::message_yesall { tit msg parent } {
    global rep_yyan
#set yes [tk_messageBox -title [ashare::mess ihm 143] -message [ashare::mess ihm 277 $valf] -type yesno -icon question -parent .]
    set fen .fen_yyan
    catch {destroy $fen}
    toplevel $fen
    wm title $fen $tit
    wm transient $fen $parent
    grab set $fen

# message
   pack [frame $fen.txt -relief raised -bd 0] -fill x -expand 1
   label $fen.txt.bla -font $astk::ihm(font,txt) -text $msg -anchor w
   pack $fen.txt.bla -padx 20 -pady 10

# choix yes / yes all / no
    set rep_yyan no
   pack [frame $fen.valid -relief solid -bd 0] -anchor c
      button $fen.valid.yes -font $astk::ihm(font,labbout) -text [ashare::mess ihm 224] -command "set rep_yyan yes ; destroy $fen ; grab release $fen"
      button $fen.valid.yea -font $astk::ihm(font,labbout) -text [ashare::mess ihm 278] -command "set rep_yyan all ; destroy $fen ; grab release $fen"
      button $fen.valid.no  -font $astk::ihm(font,labbout) -text [ashare::mess ihm 225] -command "set rep_yyan no  ; destroy $fen ; grab release $fen"
   pack $fen.valid.yes $fen.valid.yea $fen.valid.no -padx 10 -pady 3 -side left

    ashare::centre_fen $fen $parent
    tkwait window $fen
    return $rep_yyan
}
