# -*- coding: utf-8 -*-

import sys
import os
import re
from distutils        import log, sysconfig
from distutils.errors import *
from distutils.util   import strtobool

from configuration.util            import remove_tree, YES_VALUES
from configuration.remove_previous import remove_previous_main
from configuration.extract_info    import get_version_number, get_parameters
from configuration.post_install    import finalize


# configuration variables :
#  dry-run
_dry_run = False
#  destination directory
_rootdir = None
#  installation prefix
_prefix = None
#  version number of previous version
_previous_version = None
#  backup directory of previous version
_backup_dir = None
# installation parameters (type dict)
_parameters = {}
# setup options (add or not deprecated scripts...)
_options = {}


def set_dry_run(argv):
   global _dry_run
   if '--dry-run' in argv or '-n' in argv:
      log.info('"dry-run" mode is on from configuration.')
      _dry_run = True


def set_options(key, value):
   global _options
   _options[key] = False
   if value in YES_VALUES:
       _options[key] = True
   log.debug("setting '%s' to '%s'", key, _options[key])

def set_dirs(rootdir, prefix):
   global _rootdir, _prefix
   _rootdir = rootdir
   if _rootdir != "":
       _rootdir = os.path.abspath(_rootdir)
   prefix = os.path.abspath(prefix)
   _prefix = prefix
   log.info("setting rootdir to %s", repr(_rootdir))
   log.info("setting prefix to %s", repr(_prefix))


def _check():
   """Check initialization and also allows main functions to stop in dry-run mode."""
   if _dry_run:
      return False
   if _prefix is None or _rootdir is None:
      raise DistutilsSetupError, "configuration.set_dirs must be called first!"
   return True


def is_already_installed():
   if not _check(): return False
   mod = os.path.join(sysconfig.get_python_lib(prefix=_prefix), 'asrun')
   dir = os.path.join(_prefix, 'ASTK')
   exists = os.path.isdir(mod) or os.path.isdir(dir)
   if exists:
      log.info('a previous installation was found.')
   else:
      log.info('no previous installation found.')
   return exists


def get_installed_version():
   global _previous_version
   if not _check(): return
   _previous_version = get_version_number(_prefix)
   return _previous_version


def remove_previous():
   global _backup_dir
   if not _check(): return
   _backup_dir = remove_previous_main(_prefix, _previous_version)
   return _backup_dir


def should_continue(question):
   try:
      resp=raw_input(question + " (y/n) ? ")
      val = strtobool(resp)
   except:
      val = False
   return val


def get_install_parameters():
   global _parameters
   _parameters = get_parameters(_previous_version, _backup_dir, _prefix)


def configure():
   global _rootdir
   if not _check() or not _parameters: return
   finalize(_rootdir, _prefix, _parameters, _previous_version, **_options)


if __name__ == '__main__':
   log.set_verbosity(3)
   set_dirs('/opt/aster')
   get_installed_version()

