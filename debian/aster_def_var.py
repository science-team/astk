   # 1.3.2. ----- convert uname value to Code_Aster terminology...
   sysname, nodename, release, version, machine = os.uname()
   log._print('Architecture : os.uname = %s' % str(os.uname()), DBG=True)
   log._print('Architecture : sys.platform = %s    os.name = %s' % (sys.platform, os.name), DBG=True)
   
   sident = ' '.join(os.uname())
   if os.path.isfile('/etc/issue'):
      sident = re.sub(r'\\.', '', open('/etc/issue', 'r').read()) + sident
   log._print(_separ, """Installation on :
%s""" % sident, _separ)

   if sys.platform == 'win32':
      cfg['IFDEF'] = 'WIN32'
   elif sys.platform.lower() == 'darwin':
      cfg['ARCH'] = 'default'
      cfg['IFDEF'] = 'LINUX'
   elif sys.platform in ('linux2', 'cygwin'):
      cfg['ARCH'] = 'x86'
      if machine.endswith('64'):
         cfg['IFDEF'] = 'LINUX64'
         if machine in ('x86_64', 'ia64'):
            cfg['ARCH'] = machine
         else: # force to x86_64
            cfg['ARCH'] = 'x86_64'
      else:
         cfg['IFDEF'] = 'LINUX'
   elif sys.platform[:4] == 'osf1':
      cfg['IFDEF']='TRU64'
   elif sys.platform == 'sunos5':
      cfg['IFDEF'] = 'SOLARIS'
   # elif sys.platform[:6] == 'irix64':
   #    cfg['IFDEF']='IRIX64'
   elif sys.platform[:4] == 'irix':
      cfg['IFDEF'] = 'IRIX'
   else:
       raise SetupError, "Unsupported platform : sys.platform=%s, os.name=%s" % \
             (sys.platform, os.name)
   if cfg.get('_solaris64', False) and sys.platform == 'sunos5':
      cfg['IFDEF'] = 'SOLARIS64'
   cfg['DEFINED'] = cfg['IFDEF']

   # 1.4.1c. ----- check for mpirun command
   #ftools.find_and_set(cfg, 'MPIRUN', ['mpirun', 'prun'], err=False)
   cfg['MPIRUN'] = cfg.get('MPIRUN', 'mpirun')

   # 1.4.4. ----- check for a terminal
   ListTerm=[
      ['xterm' , 'xterm -display @D -e @E',],
      ['gnome-terminal' , 'gnome-terminal --display=@D --command=@E',],
      ['konsole', 'konsole --display @D -e @E'],]
   for prg, cmd in ListTerm:
      term = ftools.find_file(prg, typ='bin')
      if term != None:
         term = cmd.replace(prg, term)
         break
   cfg['TERMINAL'] = cfg.get('TERMINAL', term)
   if cfg['TERMINAL'] is None:
      del cfg['TERMINAL']

   # 1.4.5. ----- check for a text editor
   # and modify command line of those which don't have a --display= option
   ListEdit=[
      ['nedit' , 'nedit',],
      ['gedit' , 'gedit --display=@D',],
      ['kwrite', 'kwrite --display @D',],
      ['xemacs', 'xemacs -display @D',],
      ['emacs' , 'emacs -display @D',],
      ['xedit' , 'xedit -display @D',],
      ['vi'    , cfg.get('TERMINAL', 'xterm')+' -e vi',],]
   for prg, cmd in ListEdit:
      edit = ftools.find_file(prg, typ='bin')
      if edit != None:
         edit = cmd.replace(prg, edit)
         break
   cfg['EDITOR'] = cfg.get('EDITOR', edit)
   if cfg['EDITOR'] == None:
      del cfg['EDITOR']

   # 1.4.6. ----- check for debugger
   #  DEBUGGER_COMMAND runs an interactive debugger
   #  DEBUGGER_COMMAND_POST dumps a post-mortem traceback
   #     @E will be remplaced by the name of the executable
   #     @C will be remplaced by the name of the corefile
   #     @D will be remplaced by the filename which contains "where+quit"
   #     @d will be remplaced by the string 'where ; quit'
   cfg['DEBUGGER_COMMAND'] = ''
   cfg['DEBUGGER_COMMAND_POST'] = ''
   ListDebbuger=[
      ['gdb',     '%s -batch --command=@D @E @C',],
      ['dbx',     '%s -c @D @E @C',],
      ['ladebug', '%s -c @D @E @C',],]
   for debugger, debugger_command_format in ListDebbuger:
      debugger_command = ftools.find_file(debugger, typ='bin')
      if debugger_command != None:
         cfg['DEBUGGER_COMMAND_POST'] = debugger_command_format % debugger_command
         break
   
   if debugger_command != None:
      ddd = ftools.find_file('ddd', typ='bin')
      if ddd != None:
         cfg['DEBUGGER_COMMAND'] = '%s --%s --debugger %s --command=@D @E @C' \
            % (ddd, debugger, debugger_command)
   
   #-------------------------------------------------------------------------------
   # 1.5. ----- products configuration
   #-------------------------------------------------------------------------------
   log._print(_separ, term='')

   # 1.5.1. ----- check for hostname (for client part of astk)
   log._print(_fmt_search % _('host name'), term='')
   host=system.GetHostName()
   # deduce domain name
   tmp=host.split('.')
   if len(tmp)>1:
      host=tmp[0]
      domain='.'.join(tmp[1:])
   else:
      domain=''
   cfg['SERVER_NAME']=cfg.get('SERVER_NAME', host)
   cfg['DOMAIN_NAME']=cfg.get('DOMAIN_NAME', domain)
   cfg['FULL_SERVER_NAME']=cfg.get('FULL_SERVER_NAME', '.'.join(tmp))
   domain=cfg['DOMAIN_NAME']
   if domain=='':
      domain='(empty)'
   log._print(cfg['SERVER_NAME'])
   log._print(_fmt_search % _('network domain name'), domain)
   log._print(_fmt_search % _('full qualified network name'), cfg['FULL_SERVER_NAME'])
   
 
