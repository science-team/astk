#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: outils.tcl 2309 2006-11-02 15:35:25Z courtois $

#################################################################
proc init_outils {} {
# outils par d�faut, connus de l'interface, autoconfigur�s...
   def_outils
# initialiser la config de l'utilisateur si besoin
   set astk::outils(nb) 0
   set fich $astk::fic_outils
   if { [ file exists $fich ] == 0 } {
      set fich [file join $ashare::astkrc_ref outils]
   }
   set iret [lire_outils $fich]
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (init_outils) retour : $iret"
   }
# pour que la suppression de l'outil BSF (maintenant dans
# outils_autocfg) soit transparente pour l'utilisateur
   set mod 0
   for {set i [expr $astk::outils(nb) - 1]} {$i >= 0} {incr i -1} {
      if { $astk::outils($i,nom) == "BSF" } {
         set mod 1
         suppr_outil_tab $i
      }
   }
   if { $mod == 1 } {
      save_outils
   }
}

#################################################################
proc init_print {} {
   set astk::ihm(print_cmde) {a2ps lpr}
# initialiser la config de l'utilisateur si besoin
   set astk::print(nb) 0
   set fich $astk::fic_print
   set iret [lire_print $fich]
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (init_print) retour : $iret"
   }
}

#################################################################
proc lire_outils { fich } {
   #set fich $astk::fic_outils
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_outils) Lecture $fich" }

   if { [ file exists $fich ] == 0 } {
      return 4
   }
   set iret [ ashare::lire_mc_val $fich mots vale nlu ]
   if { $iret != 0 } {
      return $iret }

   # phase de v�rif
   # mots-cl�s reconnus
   set mots_cles $ashare::mots(MCF_out)
   # mots-cl�s sous le mot-cl� facteur
   set mots_smcf $ashare::mots(SSF_out)
   append mots_cles $mots_smcf
   # les mots-cl�s sont obligatoires
   set pmcs [llength $mots_smcf]
   set pmcf [expr [llength $mots_cles] - $pmcs - 1]
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set pres($mcs) 0
   }

   set nbmcf -1
   set nberr 0
   for {set j 1} {$j <= $nlu} {incr j} {
      #astkrc_version
      if { $mots($j) == "astkrc_version" } {
         if { $vale($j) != "$ashare::astkrc_version" } {
            ashare::mess "erreur" 2 $fich
            return PB_astkrc_version
         }
      } else {
         set iv [lsearch -exact $mots_cles $mots($j)]
         if { $iv < 0 } {
            ashare::mess "erreur" 5 $mots($j)
            incr nberr
         } else {
            incr pres($mots($j))
            if { $mots($j) == "nom" } {
               incr nbmcf
               set astk::outils($nbmcf,$mots($j)) $vale($j)
            } else {
               set astk::outils($nbmcf,$mots($j)) $vale($j)
            }
         }
      }
   }
   if { $nbmcf < 0 } {
      ashare::mess "erreur" 7 "outil" "tool" ""
   }
   incr nbmcf
   set astk::outils(nb) $nbmcf
   # est-ce qu'il manque un mot-cl� ?
   set sum 0
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set sum [expr $sum + $pres($mcs)]
   }
   if { $sum != [expr $pmcf+($nbmcf*($pmcs+1))] } {
      ashare::mess "erreur" 6 "?" "?" $fich ""
      incr nberr
   }
   if { $nberr > 0 } {
      ashare::mess "erreur" 8 $fich
      ashare::my_exit 4
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_outils) $nbmcf outils" }
   return 0
}

# Sauvegarde de la config des outils
#################################################################
proc save_outils { } {
   set id [open $astk::fic_outils w]
   puts $id "# AUTOMATICALLY GENERATED - DO NOT EDIT !"
   puts $id "astkrc_version : $ashare::astkrc_version"
   puts $id "#"
   set mots_cles $ashare::mots(MCF_out)
   append mots_cles $ashare::mots(SSF_out)
   for { set i 0 } { $i < $astk::outils(nb) } { incr i } {
      for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
         set mcs [lindex $mots_cles $k]
         set val  $astk::outils($i,$mcs)
         if { $val == "" } {
            set val "_VIDE"
         }
         set val [unexpandvars $val]
         puts $id "$mcs : $val"
      }
      puts $id "#"
   }
   close $id
   return 0
}

#################################################################
proc lire_print { fich } {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_print) Lecture $fich" }

   if { [ file exists $fich ] == 0 } {
      return 4
   }
   set iret [ ashare::lire_mc_val $fich mots vale nlu ]
   if { $iret != 0 } {
      return $iret }

   # phase de v�rif
   # mots-cl�s reconnus
   set mots_cles $ashare::mots(MCF_pr)
   # mots-cl�s sous le mot-cl� facteur
   set mots_smcf $ashare::mots(SSF_pr)
   append mots_cles $mots_smcf
   # les mots-cl�s sont obligatoires
   set pmcs [llength $mots_smcf]
   set pmcf [expr [llength $mots_cles] - $pmcs - 1]
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set pres($mcs) 0
   }

   set nbmcf -1
   set nberr 0
   for {set j 1} {$j <= $nlu} {incr j} {
      #astkrc_version
      if { $mots($j) == "astkrc_version" } {
         if { $vale($j) != "$ashare::astkrc_version" } {
            ashare::mess "erreur" 2 $fich
            return PB_astkrc_version
         }
      } else {
         set iv [lsearch -exact $mots_cles $mots($j)]
         if { $iv < 0 } {
            ashare::mess "erreur" 5 $mots($j)
            incr nberr
         } else {
            incr pres($mots($j))
            if { $mots($j) == "label" } {
               incr nbmcf
               set astk::print($nbmcf,$mots($j)) $vale($j)
            } else {
               set astk::print($nbmcf,$mots($j)) $vale($j)
            }
         }
      }
   }
   if { $nbmcf < 0 } {
      ashare::mess "erreur" 7 "imprimante" "printer" "e"
   }
   incr nbmcf
   set astk::print(nb) $nbmcf
   # est-ce qu'il manque un mot-cl� ?
   set sum 0
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set sum [expr $sum + $pres($mcs)]
   }
   if { $sum != [expr $pmcf+($nbmcf*($pmcs+1))] } {
      ashare::mess "erreur" 6 "?" "?" $fich ""
      incr nberr
   }
   if { $nberr > 0 } {
      ashare::mess "erreur" 8 $fich
      ashare::my_exit 4
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_print) $nbmcf imprimantes" }
   return 0
}

# Sauvegarde de la config des imprimantes
#################################################################
proc save_print { } {
   set id [open $astk::fic_print w]
   puts $id "# AUTOMATICALLY GENERATED - DO NOT EDIT !"
   puts $id "astkrc_version : $ashare::astkrc_version"
   puts $id "#"
   set mots_cles $ashare::mots(MCF_pr)
   append mots_cles $ashare::mots(SSF_pr)
   for { set i 0 } { $i < $astk::print(nb) } { incr i } {
      for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
         set mcs [lindex $mots_cles $k]
         set val  $astk::print($i,$mcs)
         if { $val == "" } {
            set val "_VIDE"
         }
         puts $id "$mcs : $val"
      }
      puts $id "#"
   }
   close $id
   return 0
}

