###########################################################
Textes param�trables en fonction de la langue.

Ils sont d�finis dans le fichier locales.txt (dans le r�p
share).

###########################################################
ashare::msg(nb_err)  = nbre de messages d'erreur
ashare::msg(nb_inf)  = nbre de messages d'info
ashare::msg(nb_ihm)  = nbre de labels d'ihm

# pour la langue FR :
ashare::msg(FR,erreur,txt) = label pour les messages d'erreur
ashare::msg(FR,info,txt)   = label pour les messages d'info
ashare::msg(FR,ihm,txt)    = pas utilis�

ashare::msg(FR,erreur,i) = message d'erreur num�ro i
ashare::msg(FR,info,i)   = message d'info num�ro i
ashare::msg(FR,ihm,i)    = label ihm num�ro i

