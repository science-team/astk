Param�tres du calcul :


--------------------
Les param�tres du calcul sont fournis dans la partie droite de la fen�tre principale.
Le menu Options donne acc�s � des param�tres de lancement suppl�mentaires.

- M�moire : Elle est donn�e en m�ga-octets (Mo), c'est la m�moire qui sera r�serv�e par le calcul Aster.

- Temps : Il peut �tre fourni en secondes (par exemple 180),
   en minutes:secondes (par exemple 10:00),
   ou en heures:minutes:secondes (par exemple 06:30:00).

- Machine d'ex�cution : Choix de la machine de calcul.

- Version : Version de Code_Aster utilis�e

- batch : lancement du calcul en mode batch (s�quenceur de travaux)

- interactif : lancement en interactif
   - suivi interactif : Permet de suivre l'avanc�e du calcul

- debug/nodebug : Permet de choisir le mode de compilation utilis�, ou le type d'ex�cutable quand celui-ci est disponible

- run/dbg/pre : En interactif, permet de choisir le mode de lancement de l'�tude :
   - run : ex�cute l'�tude (fonctionnement classique),
   - dbg : lance l'�tude en utilisant le debugger,
   - pre : pr�pare le r�pertoire de travail sans ex�cuter l'�tude.

--------------------
Param�tres suppl�mentaires :

- Nombre de processeurs : Permet de positionner la variable d'environnement qui active le calcul sur plusieurs processeurs (parall�lisme de type OpenMP pour le solveur).

- Classe batch : On peut choisir la classe batch (ou le groupe de classe) dans laquelle le calcul sera soumis. Il faut bien �videmment v�rifier que la classe existe et que les param�tres temps et m�moire sont compatibles avec cette classe.

- Heure de d�part : Permet de diff�rer le d�part d'un calcul. L'heure de d�part est fournie au format heure:minutes ou jour:heure:minutes ou mois:jour:heure:minutes.


# $Id: item_111.txt 555 2003-12-12 14:16:05Z mcourtoi $

