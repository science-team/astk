# -*- coding: utf-8 -*-

import sys
import os
import re
from pprint import pformat, pprint
from distutils import log
from distutils import sysconfig
from configuration.util import execute, get_function, re_search, check_and_store, read_rcfile, prefix2etc


def get_version_number(prefix):
   # try for version >= 1.8
   version = get_version_number_last(prefix)
   if version:
      log.info("previous installed version is %s (>= 1.8)", version)
      return version
   # try for version >= 1.7.5
   version = get_version_number_1_7_5(prefix)
   if version:
      log.info("previous installed version is %s (>= 1.7.5)", version)
      return version
   # try for version <= 1.7.4
   version = get_version_number_older(prefix)
   if version:
      log.info("previous installed version is %s", version)
      return version
   if not version:
      log.warn("warning: can not get currently installed version!")
   return version


def get_version_number_last(prefix):
   version = None
   try:
      path = sysconfig.get_python_lib(prefix=prefix)
      cmd = [sys.executable, '-c',
             "import sys ; sys.path.insert(0, '%s') ; import asrun ; "\
             "print 'as_run # %%s # %%s' %% (asrun.__version__, asrun.__file__)" % path]
      output = execute(cmd)
      log.debug("get_version_number >= 1.8, output :\n%s", output)
      out = output.split('#')
      if len(out) != 3:
          return version
      version, asrfile = [s.strip() for s in out[1:3]]
      asrfile = re.sub("/asrun/__init__.py.*$", "", asrfile)
      if asrfile.replace(path, '') != '':
          raise Exception("unexpected asrun found :%s" % asrfile)
   except Exception, msg:
      log.debug("get_version_number >= 1.8, message : %s", str(msg))
   return version


def get_version_number_1_7_5(prefix):
   version = None
   try:
      path = os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'lib')
      assert os.path.exists(os.path.join(path, '__pkginfo__.py')), 'module not found: %s' % path
      cmd = [sys.executable, '-c',
             "import sys ; sys.path.insert(0, '%s') ; import __pkginfo__ as p ; print 'as_run', p.version" % path]
      output = execute(cmd)
      log.debug("get_version_number >= 1.7.5, output :\n%s", output)
      mat = re.search('as_run +([0-9\-\.A-Za-z_]+)', output)
      version = mat.group(1)
   except Exception, msg:
      log.debug("get_version_number >= 1.7.5, message : %s", str(msg))
   return version


def get_version_number_older(prefix):
   version = None
   try:
      context = { 'path' : os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'bin') }
      cmd = ['%(path)s/as_run' % context, '--version']
      output = execute(cmd)
      log.debug("get_version_number <= 1.7.4, output :\n%s", output)
      mat = re.search('as_run +([0-9\-\.A-Za-z_]+)', output)
      version = mat.group(1)
   except Exception, msg:
      log.debug("get_version_number <= 1.7.4, message : %s", str(msg))
   return version


def get_parameters(version, where, prefix=None):
   """Return the dict of installation parameters.
   """
   if where is None:
      where = ''
   if prefix is None:
      prefix = where
   if not version:
      version = 'unknown'
   log.info("searching for installation parameters of version %s...", version)
   log.debug("          from %s", where)
   log.debug("   destination %s", prefix)
   # default values
   parameters = {
      'ASTER_ROOT'      : prefix,
      'ASTER_VERSION'   : 'testing',
      'SHELL_EXECUTION' : '/bin/bash',
      'WISH_EXE'        : 'wish',
      'PYTHON_EXE'      : sys.executable,
      'HOME_PYTHON'     : sys.prefix,
      'server_confdir'  : '',
      'server_conf_values' : {},
      'client_confdir'  : '',
   }
   try:
      from external_configuration import parameters as ext_param
      assert type(ext_param) is dict, 'invalid type for external_configuration dict : %s' % type(ext_param)
      parameters.update(ext_param)
   except (ImportError, AssertionError), error:
      log.info(str(error))
      pass
   parameters.update({
      'TOOLS_DIR'      : os.path.join('$ASTER_ROOT', 'outils'),
      'ASRUN_SITE_PKG' : sysconfig.get_python_lib(prefix='$ASTER_ROOT'),
   })
   if where:
      # SHELL_EXECUTION
      func_shell(parameters, where, version)
      # WISH_EXE
      func_wish(parameters, where, version)
      # add config files location
      func_config_files(parameters, where, version)
      # read config file
      func_config(parameters, where, version)
      # for auto_update.cron
      func_config_auto_update(parameters, where)
   # print dict
   log.info("parameters values :")
   pprint(parameters)
##   log.info(pformat(parameters))  # python-Bugs-1541642
   return parameters


def func_shell(param, where, version):
   files = ( ('0.0.0', os.path.join(where, 'ASTK', 'ASTK_SERV', 'bin', 'as_run')),
             ('1.8.0', os.path.join(where, 'bin', 'as_run')) )
   mat = re_search(get_function(files, version), '^#!(.*)')
   check_and_store(param, mat, 'SHELL_EXECUTION')


def func_wish(param, where, version):
   files = ( ('0.0.0', os.path.join(where, 'ASTK', 'ASTK_CLIENT', 'bin', 'astk')),
             ('1.8.0', os.path.join(where, 'etc', 'codeaster', 'profile.sh')) )
   expressions = ( ('0.0.0', '(.*) +.*/ASTK/ASTK_CLIENT/lib/ASTK/astk.tcl'),
                   ('1.8.0', 'WISHEXECUTABLE=(.*)') )
   mat = re_search(get_function(files, version), get_function(expressions, version))
   check_and_store(param, mat, 'WISH_EXE')


def func_config(param, where, version):
   """Store values to configure each file ('new' in the loop)"""
   confdir = param['server_confdir']
   if not confdir:
      return
   per_file = param['server_conf_values']
   for new, config_file in param['server_config_files'].items():
      if not os.path.isfile(config_file):
         log.warn("warning: file not found: %s", config_file)
         continue
      log.info("reading configuration file: %s", config_file)
      per_file[new] = {}
      read_rcfile(config_file, per_file[new])
   # if the philosophy of a field changes, it should be written here.

def func_config_auto_update(param, where):
    """Store the parameters set in auto_update.cron"""
    dico = param['server_conf_values']['auto_update.cron'] = {}
    fname = os.path.join(where, 'bin', 'auto_update.cron')
    for var in ('VERSIONS_TO_UPDATE', 'VERSIONS_TO_BUILD', 'MAIL_DEST'):
        mat = re_search(fname, '%s=(.*)' % var)
        check_and_store(dico, mat, var)

def func_config_files(param, where, version):
   server = ( ('0.0.0', os.path.join(where, 'ASTK', 'ASTK_SERV', 'conf')),
              ('1.8.0', os.path.join(prefix2etc(where), 'codeaster')) )
   client = ( ('0.0.0', os.path.join(where, 'ASTK', 'ASTK_CLIENT', 'lib', 'ASTK', 'astkrc')),
              ('1.8.0', os.path.join(prefix2etc(where), 'codeaster', 'astkrc')) )
   param['server_confdir'] = get_function(server, version)
   param['client_confdir'] = get_function(client, version)
   # server
   files = ('asrun', 'aster', '.mysql_connect_REX')
   exceptions = ( ('0.0.0', { 'asrun' : 'config',
                              'aster' : 'config', }),
                  ('1.8.0', {}) )
   dchg = get_function(exceptions, version)
   dpair = {}
   for name in files:
      dpair[name] = os.path.join(param['server_confdir'], dchg.get(name, name))
   param['server_config_files'] = dpair

   # client
   files = ('config_serveurs', 'outils', 'prefs')
   exceptions = ( ('0.0.0', {}),
                  ('1.8.0', {}) )
   dchg = get_function(exceptions, version)
   dpair = {}
   for name in files:
      dpair[os.path.join('astkrc', name)] = \
               os.path.join(param['client_confdir'], dchg.get(name, name))
   param['client_config_files'] = dpair


