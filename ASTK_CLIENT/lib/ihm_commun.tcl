#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_commun.tcl 3649 2008-11-28 16:01:22Z courtois $

# d�finition du nom des onglets et sous-onglet
#################################################################
proc def_onglet { } {
   set n 1
   set i 0
   incr i
   set astk::ihm(ong,$n,$i) "etude"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 6]
   set astk::ihm(ctx,$n,$i) 282
   set astk::ihm(coch,$n)  1
   set astk::ihm(nbong,$n) $i
   incr n
   set i 0
   incr i
   set astk::ihm(ong,$n,$i) "tests"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 7]
   set astk::ihm(ctx,$n,$i) 283
   set astk::ihm(coch,$n)  1
   set astk::ihm(nbong,$n) $i
   incr n
   set i 0
   incr i
   set astk::ihm(ong,$n,$i) "surcharge"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 8]
   set astk::ihm(ctx,$n,$i) 284
   set astk::ihm(coch,$n)  1
   set astk::ihm(nbong,$n) $i
   set n $astk::ihm(nongl_agla)
   set i 0
   incr i
   set astk::ihm(ong,$n,$i) "agla"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 10]
   set astk::ihm(ctx,$n,$i) 285
   incr i
   set astk::ihm(ong,$n,$i) "asno"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 9]
   set astk::ihm(ctx,$n,$i) 286
   incr i
   set astk::ihm(ong,$n,$i) "asdeno"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 144]
   set astk::ihm(ctx,$n,$i) 287
   incr i
   set astk::ihm(ong,$n,$i) "asdenot"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 428]
   set astk::ihm(ctx,$n,$i) 287
   incr i
   set astk::ihm(ong,$n,$i) "asquit"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 152]
   set astk::ihm(ctx,$n,$i) 288
   incr i
   set astk::ihm(ong,$n,$i) "asverif"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 145]
   set astk::ihm(ctx,$n,$i) 289
   incr i
   set astk::ihm(ong,$n,$i) "pre_eda"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 146]
   set astk::ihm(ctx,$n,$i) 290
   incr i
   set astk::ihm(ong,$n,$i) "asrest"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 147]
   set astk::ihm(ctx,$n,$i) 291
   set astk::ihm(coch,$n)  1
   set astk::ihm(nbong,$n) $i
   incr n
   set i 0
   incr i
   set astk::ihm(ong,$n,$i) "rex"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 11]
   set astk::ihm(ctx,$n,$i) 294
   incr i
   set astk::ihm(ong,$n,$i) "emis_sans"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 210]
   set astk::ihm(ctx,$n,$i) 296
   incr i
   set astk::ihm(ong,$n,$i) "emis_prof"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 211]
   set astk::ihm(ctx,$n,$i) 297
   incr i
   set astk::ihm(ong,$n,$i) "consult"
   set astk::ihm(tit,$n,$i) [ashare::mess ihm 243]
   set astk::ihm(ctx,$n,$i) 298
   set astk::ihm(coch,$n)  0
   set astk::ihm(nbong,$n) $i
#
   set astk::ihm(nbongM)  $n
}

# cree les onglets
# si REINI = "DETR", on d�truit l'onglet avant de le recr�er
#################################################################
proc init_onglet { {REINI "INI"} } {
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      if { $REINI == "DETR" } {
         destroy $astk::ihm(fenetre).onglet.$astk::ihm(ong,$i,1)
      }
      fenEEC $astk::ihm(fenetre).onglet.$astk::ihm(ong,$i,1) $i
   }
}

# cree une liste d�roulante d'onglet
# nongl : num�ro de l'onglet principal
#################################################################
proc fenEEC { fenetre_parent nongl } {
   global ongsel
   pack [frame $fenetre_parent] -side left -padx 5
   pack [frame $fenetre_parent.titre -relief raised -bd 1 -background $astk::ihm(couleur,onglet_inactif)]
   set astk::ihm(ssong,$nongl) 1
   if { $astk::ihm(nbong,$nongl) > 1 } {
      set MenuButt [tk_optionMenu $fenetre_parent.titre.songl ongsel($nongl) $astk::ihm(tit,$nongl,1)]
      $MenuButt configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
      $MenuButt entryconfigure 1 -font $astk::ihm(font,labbout) -command "choix_ongletM $nongl 1"
      set ashare::context($MenuButt,0) $astk::ihm(ctx,$nongl,1)
      for {set j 2} {$j <= $astk::ihm(nbong,$nongl)} {incr j} {
         $MenuButt add radiobutton
         $MenuButt entryconfigure $j -label $astk::ihm(tit,$nongl,$j) -font $astk::ihm(font,labbout) -variable ongsel($nongl) -command "choix_ongletM $nongl $j"
         set j1 [expr $j - 1]
         set ashare::context($MenuButt,$j1) $astk::ihm(ctx,$nongl,$j)
      }
      bind $MenuButt <<MenuSelect>> {ShowStatus menu %W astk}
   } else {
      set ongsel($nongl) $astk::ihm(tit,$nongl,1)
      button $fenetre_parent.titre.songl -text $astk::ihm(tit,$nongl,1) -font $astk::ihm(font,labbout) -command "choix_ongletM $nongl 1"
   }
   $fenetre_parent.titre.songl configure -relief raised -font $astk::ihm(font,labbout) -background $astk::ihm(couleur,onglet_inactif) -activebackground $astk::ihm(couleur,onglet_inactif)
   pack $fenetre_parent.titre.songl -side left
   if { $astk::ihm(coch,$nongl) } {
      checkbutton $fenetre_parent.titre.choix \
         -offvalue non    \
         -onvalue oui      \
         -variable astk::profil(M_$nongl) \
         -command "coche_ongletM $nongl" \
         -background $astk::ihm(couleur,onglet_inactif) \
         -activebackground $astk::ihm(couleur,onglet_inactif)
      pack $fenetre_parent.titre.choix -side right
   }
   # onglets inactiv�s
   if { $astk::ihm(ong,$nongl,1) == "agla" && $astk::agla(status) != "on" } {
      $fenetre_parent.titre.songl configure -state disabled
      $fenetre_parent.titre.choix configure -state disabled
      set astk::profil($astk::ihm(ong,$nongl,1)) non
   }
   # rex : desactiver si pas acc�s � mach_ref
   if { ($astk::ihm(ong,$nongl,1) == "rex") &&  ($astk::agla(num_serv) == -1) } {
      $fenetre_parent.titre.songl configure -state disabled
   }
# aide contextuelle
   def_context $fenetre_parent.titre.songl $astk::ihm(ctx,$nongl,1)
}

# n'est plus utilis�e avec la case � cocher
#################################################################
proc fenEEC2 {fenetre_parent chaine {vari titre} {coch 0}} {
   pack [frame $fenetre_parent.titre -relief solid -bd 1 -background $astk::ihm(couleur,onglet_inactif)]
   label $fenetre_parent.titre.label -font $astk::ihm(font,labbout) -text $chaine  -background $astk::ihm(couleur,onglet_inactif)
   pack $fenetre_parent.titre.label -side left
   if { $coch } {
      checkbutton $fenetre_parent.titre.choix \
         -offvalue non    \
         -onvalue oui      \
         -variable astk::profil($vari) \
         -background $astk::ihm(couleur,onglet_inactif) \
         -activebackground $astk::ihm(couleur,onglet_inactif)
      pack $fenetre_parent.titre.choix -side right
   }
}

# g�re l'affichage des onglets
#################################################################
proc affiche_onglet { nom } {
   global ongsel
   set old $astk::profil(onglet_actif)
   destroy $astk::ihm(fenetre).active
   if { $nom != "none" } {
      affiche_$nom
   }

   # onglet "pere" � d�sactiver
   set ongP_off "X"
   # onglet "pere" � activer
   set ongP_on "X"
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      for {set j 1} {$j <= $astk::ihm(nbong,$i)} {incr j} {
         if { $astk::profil(onglet_actif) == $astk::ihm(ong,$i,$j) } {
            set ongP_off $astk::ihm(ong,$i,1)
         }
         if { $nom == $astk::ihm(ong,$i,$j) } {
            set ongP_on $astk::ihm(ong,$i,1)
         # pour positionner la liste d�roulante si on force l'affichage d'un onglet
            set ongsel($i) $astk::ihm(tit,$i,$j)
         }
      }
   }

   $astk::ihm(fenetre).onglet.$ongP_off.titre.songl configure -relief raised -background $astk::ihm(couleur,onglet_inactif) -activebackground $astk::ihm(couleur,onglet_inactif)
   # on s'arrete l� si none
   if { $nom == "none" } { return }
   set astk::profil(onglet_actif) $nom
   $astk::ihm(fenetre).onglet.$ongP_on.titre.songl configure -relief sunken -background $astk::ihm(couleur,onglet_actif) -activebackground $astk::ihm(couleur,onglet_actif)

   # status
   change_status [ashare::mess ihm 12]
#
   raffr_agla_surch
   raffr_args_fixe
}

# choix du sous-onglet ssong de l'onglet maitre nongl
#################################################################
proc choix_ongletM { nongl ssong } {
   global ongsel
   set astk::ihm(ssong,$nongl) $ssong
   verif_ongletM
# affichage sauf pour l'agla et le rex (qui ne sont pas des onglets)
   if { $astk::ihm(ong,$nongl,1) != "agla" && $astk::ihm(ong,$nongl,1) != "rex" } {
      affiche_onglet $astk::ihm(ong,$nongl,$ssong)
# actions du menu rex
   } elseif { $astk::ihm(ong,$nongl,1) == "rex" } {
      choix_rex $astk::ihm(ong,$nongl,$ssong)
      set ongsel($nongl) $astk::ihm(tit,$nongl,1)
   }
# pour l'agla, pas d'action associ�e au choix
}

# r�gle les conflits entre les onglets coch�s
# ongletM : onglet "master"
#################################################################
proc coche_ongletM { nongl } {
   set astk::profil($astk::ihm(ong,$nongl,$astk::ihm(ssong,$nongl))) $astk::profil(M_$nongl)
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (cochet_onglet) appel de coche_onglet_$astk::ihm(ong,$nongl,$astk::ihm(ssong,$nongl))\n        profil($astk::ihm(ong,$nongl,$astk::ihm(ssong,$nongl))) = $astk::profil($astk::ihm(ong,$nongl,$astk::ihm(ssong,$nongl)))"
   }
   # pour l'agla, il n'y a que l'onglet maitre qui se coche
   if { $astk::ihm(ong,$nongl,1) == "agla" } {
      set astk::profil(agla) $astk::profil(M_$nongl)
      coche_onglet_agla
   } else {
      coche_onglet_$astk::ihm(ong,$nongl,$astk::ihm(ssong,$nongl))
   }
   verif_ongletM
}

# coche/decoche les onglets maitres (cochables) si on a coch�/d�coch� le sous-onglet actif
#################################################################
proc verif_ongletM { } {
   # pour l'agla, les sous-onglets sont coch�s comme l'onglet maitre
   for {set j 2} {$j <= $astk::ihm(nbong,$astk::ihm(nongl_agla)) } {incr j} {
      set astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$j)) non
   }
   set astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$astk::ihm(ssong,$astk::ihm(nongl_agla)))) $astk::profil(agla)
   # onglets maitres fonction du sous-onglet actif
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      if { $astk::ihm(coch,$i) } {
         if { $ashare::dbg >= 4 } {
            ashare::log "<DEBUG> (verif_ongletM) profil($astk::ihm(ong,$i,$astk::ihm(ssong,$i))) = $astk::profil($astk::ihm(ong,$i,$astk::ihm(ssong,$i)))"
         }
         if { $astk::profil($astk::ihm(ong,$i,$astk::ihm(ssong,$i))) == "non" } {
            set astk::profil(M_$i) "non"
         } else {
            set astk::profil(M_$i) "oui"
         }
      }
   }
}
#############################
proc coche_onglet_etude { } {
   if { $astk::profil(etude) == "non" } {
      # quand on decoche etude
      mod_lancement disabled
   } else {
      # quand on coche etude
      set astk::profil(agla) non
      coche_onglet_agla
      set astk::profil(tests) non
      adapt_satellite 0
      mod_lancement normal
   }
}
proc coche_onglet_tests { } {
   if { $astk::profil(tests) == "non" } {
      # quand on decoche tests
      set astk::profil(agla) non
      coche_onglet_agla
      mod_lancement normal
   } else {
      # quand on coche tests
      set astk::profil(agla) non
      coche_onglet_agla
      set astk::profil(etude) non
      adapt_satellite 0
      mod_lancement disabled
   }
}
proc coche_onglet_surcharge { } {
   if { $astk::profil(surcharge) == "non" } {
      # quand on decoche surcharge
      set astk::profil(agla) non
      coche_onglet_agla
   } else {
      # quand on coche surcharge
   }
}
proc coche_onglet_agla { } {
   if { $astk::profil(agla) == "non" } {
      # quand on decoche agla
      adapt_satellite 0
      raffr_agla_surch
   } else {
      # quand on coche agla
      set astk::profil(surcharge) oui
      set astk::profil(etude) non
      set astk::profil(tests) oui
      adapt_satellite 1
      raffr_agla_surch
   }
}

# arguments pour l'exec dans etude et execution
#################################################################
proc affiche_arg { fen } {
   pack [frame $fen.argu -relief raised -bd 0 ] -pady 4 -padx 5 -side bottom
      pack [label $fen.argu.lbl -font $astk::ihm(font,lab) -font $astk::ihm(font,lab) -text [ashare::mess ihm 13]] -side left
      pack [entry $fen.argu.txt -width 20 -font $astk::ihm(font,val) -textvariable astk::profil(args)] -side left
      pack [entry $fen.argu.tx2 -width 25 -font $astk::ihm(font,val) -textvariable astk::profil(args_fixe) -state disabled] -side left
      $fen.argu.txt configure -validate key -vcmd modprof
   def_context $fen.argu.txt 281
   def_context $fen.argu.tx2 361
}

# modifie un argument
# opt : nom de l'option
# arg : si oui, on demande une valeur
#################################################################
proc mod_argument { opt arg } {
   global getValue_val

   if { $arg == "oui" && $astk::profil(option,$opt) } {
      set last $astk::profil(opt_val,$opt)
      getValue_fen . 24 13 {} $last non_vide
      tkwait window .fen_getv
      if { $getValue_val != "_VIDE" } {
         set astk::profil(opt_val,$opt) $getValue_val
      } else {
         set astk::profil(option,$opt) 0
      }
   }
   raffr_args_fixe
}

# actualise la zone arguments fixes
#################################################################
proc raffr_args_fixe { } {
    set astk::profil(args_fixe) ""
    set narg [expr [llength $astk::ihm(l_arg)] / 3]
    for {set i 0} {$i < $narg} {incr i} {
      set arg  [lindex $astk::ihm(l_arg) [expr $i * 3]]
      if { $astk::profil(option,$arg) == 1 } {
         append astk::profil(args_fixe) "-$arg $astk::profil(opt_val,$arg) "
      }
    }
}


# modifie un param�tre
#################################################################
proc mod_param { m opt } {
   global getValue_val
# valeurs autoris�es
   set npar [expr [llength $astk::ihm(l_par)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set var [lindex $astk::ihm(l_par) [expr $i * 3]]
      if { $var == $opt } {
         set lst [lindex $astk::ihm(l_par) [expr $i * 3 + 2]]
         break
      }
   }
# demande la valeur
   set last $astk::profil(opt_val,$opt)
   if { [lindex $lst 0 ] == "OUI" } {
       getValue_chb . 24 $opt $lst $last
   } else {
       getValue_fen . 24 267 {} $last vide 1
   }
   tkwait window .fen_getv
   if { $getValue_val != "_VIDE" } {
      if { [lsearch -exact $lst $getValue_val] > -1 || [llength $lst] == 0 } {
         set astk::profil(opt_val,$opt) $getValue_val
         $m entryconfigure "$opt = $last" -label "$opt = $astk::profil(opt_val,$opt)"
      } else {
         set msg [ashare::mess erreur 5 $getValue_val]
         change_status $msg
         tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info -parent .
      }
   }
}

# modifie une option de lancement
#################################################################
proc mod_option { m opt } {
   global getValue_val
# valeurs autoris�es
   set npar [expr [llength $astk::ihm(l_opt)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set var [lindex $astk::ihm(l_opt) [expr $i * 3]]
      if { $var == $opt } {
         set lst [lindex $astk::ihm(l_opt) [expr $i * 3 + 2]]
         break
      }
   }
# demande la valeur
   set last $astk::profil(opt_val,$opt)
   if { [lindex $lst 0 ] == "OUI" } {
       getValue_chb . 24 $opt $lst $last
   } else {
       getValue_fen . 24 267 {} $last vide 1
   }
   tkwait window .fen_getv
   if { $getValue_val == "" || [lsearch -exact $lst $getValue_val] > -1 || [llength $lst] == 0 } {
      set astk::profil(opt_val,$opt) $getValue_val
      $m entryconfigure "$opt = $last" -label "$opt = $astk::profil(opt_val,$opt)"
   } else {
      set msg [ashare::mess erreur 5 $getValue_val]
      change_status $msg
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info -parent .
   }
}

# aide contextuelle
# type : menu ou bouton
# wmem : widget menu
# prog : astk ou asjob (pour savoir quelle variable on modifie)
#################################################################
proc ShowStatus { typ wmen prog {etat "on"}} {
   switch -exact -- $typ {
      menu {
         set ind [$wmen index active]
         set varst [ashare::mess ihm 12]
         catch {set varst [ashare::mess ihm $ashare::context($wmen,$ind)]}
      }
      bouton {
         set varst $ashare::context($wmen,bouton)
      }
   }
   if { $etat == "off" } {
      set varst ""
   }
   switch -exact -- $prog {
      astk {
         change_status $varst
      }
      asjob {
         set asjob::status $varst
      }
      bsf {
         set tkgetdir::status $varst
      }
   }
   update idletasks
}

# pour simplifier l'�criture de l'aide sur un widget survol�
# w   : widget (�v�nements Enter/Leave)
# num : num�ro du message
#################################################################
proc def_context { w num } {
   set ashare::context($w,bouton) [ashare::mess ihm $num]
   bind $w <Enter> {ShowStatus bouton %W astk}
   bind $w <Leave> {ShowStatus bouton %W astk off}
}

# cr�e la liste des derniers profils ouverts
#################################################################
proc dern_prof { parent } {
   if { $astk::ihm(prof,nb) > $astk::config(-1,nb_reman) } {
      set astk::ihm(prof,nb) $astk::config(-1,nb_reman)
   }
# longueur max (-3 pour ...)
   set smax 37
   if { $astk::ihm(prof,nb) > 0 } {
      $parent add separator
   }
   for {set i 0} {$i < $astk::ihm(prof,nb)} {incr i} {
      set lbl [expr $i + 1]
      append lbl " "
      append lbl $astk::ihm(prof,serv,$i)
      append lbl ":"
      set fich $astk::ihm(prof,$i)
      set smax2 [expr $smax - [string length $astk::ihm(prof,serv,$i)]]
      set sf [string length $fich]
      if { $sf > $smax2 } {
         append lbl "..."
         set fich [string range $fich [expr $sf - $smax2] [expr $sf - 1]]
      }
      append lbl $fich
      $parent add command -font $astk::ihm(font,labmenu) -label $lbl -underline 0 -command "ouvrir $astk::inv(serv,$astk::ihm(prof,serv,$i)) $astk::ihm(prof,$i)"
   }
}

# mise � jour de la liste des derniers profils ouverts
# apr�s Fichier/Ouvrir - Enregistrer sous...
# sans profil, on raffraichit simplement le menu
#################################################################
proc maj_prof { {serv -1 } { new "_VIDE" } } {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (maj_prof) profil : $new"
   }
   if { $new != "_VIDE" && $new != [ashare::mess ihm 27] } {
      set nb $astk::ihm(prof,nb)
      for {set i 0} {$i < $nb} {incr i} {
         set oldp($i) $astk::ihm(prof,$i)
         set olds($i) $astk::ihm(prof,serv,$i)
      }
# le dernier doit apparaitre en premier
      set astk::ihm(prof,0) $new
      set astk::ihm(prof,serv,0) $astk::config($serv,nom)
      set newnb 0
      for {set i 0} {$i < $nb} {incr i} {
         if { [regexp $new $oldp($i)] == 0 } {
            incr newnb
            set astk::ihm(prof,$newnb) $oldp($i)
            set astk::ihm(prof,serv,$newnb) $olds($i)
         }
         if { $newnb >= [expr $astk::config(-1,nb_reman) - 1] } {
            break
         }
      }
      incr newnb
      set astk::ihm(prof,nb) $newnb
# sauvegarde la liste
      set id [open $astk::fic_prof w]
      puts $id "# AUTOMATICALLY GENERATED - DO NOT EDIT !"
      puts $id "#"
      for { set i 0 } { $i < $astk::ihm(prof,nb) } { incr i } {
         puts $id "profil : $astk::ihm(prof,$i)"
         puts $id "serv : $astk::ihm(prof,serv,$i)"
      }
      close $id
   }
# mise � jour du menu
   create_menu_fichier $astk::ihm(menu) DETR
}

# cr�e la liste des noeuds
#################################################################
proc liste_machines { parent } {
   set pastrouve 1
   catch { destroy $parent.noeud }
   set MenuNoeud [tk_optionMenu $parent.noeud astk::profil(noeud) $astk::ihm(lnoeud,0)]
   $MenuNoeud configure \
       -foreground $astk::ihm(couleur,menu_foreground) \
       -background $astk::ihm(couleur,menu_background)
   $MenuNoeud entryconfigure 0 -font $astk::ihm(font,lablst) -command "change_noeud"
   for {set j 1} {$j < $astk::ihm(nb_noeud)} {incr j} {
      $MenuNoeud add radiobutton
      $MenuNoeud entryconfigure $j -label $astk::ihm(lnoeud,$j) -font $astk::ihm(font,lablst) -variable astk::profil(noeud) -command "change_noeud"
      if { $astk::profil(noeud) == $astk::ihm(lnoeud,$j) } {
         set pastrouve 0
      }
   }
   $parent.noeud configure -font $astk::ihm(font,lablst)
   pack $parent.noeud -fill x -expand 1
   # on repositionne sur le premier si la machine selectionnee n'existe plus !
   if { $pastrouve } {
      set astk::profil(noeud) $astk::ihm(lnoeud,0)
   }
# aide contextuelle
   def_context $parent.noeud 312
}

# cr�e la liste des versions
#################################################################
proc liste_versions { parent } {
   set pastrouve 1
   set trouvedef 0
   catch { destroy $parent.vers }
   catch {
      if { $astk::profil(version) == $astk::ihm(lvers,0) } { set pastrouve 0 }
   }
   set MenuVers [tk_optionMenu $parent.vers astk::profil(version) $astk::ihm(lvers,0)]
   $MenuVers configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuVers entryconfigure 0 -font $astk::ihm(font,lablst)
   for {set j 1} {$j < $astk::ihm(nb_vers)} {incr j} {
      $MenuVers add radiobutton
      $MenuVers entryconfigure $j -label $astk::ihm(lvers,$j) -font $astk::ihm(font,lablst) -variable astk::profil(version)
      if { $astk::profil(version) == $astk::ihm(lvers,$j) } {
         set pastrouve 0
      }
      if { $astk::config(-1,def_vers) == $astk::ihm(lvers,$j) } {
         set trouvedef 1
      }
   }
   $parent.vers configure -font $astk::ihm(font,lablst)
   pack $parent.vers -fill x -expand 1
   # si la version actuelle n'a pas �t� trouv�e et que
   # la version pr�f�r�e de l'utl est dispo, on la prend, sinon version 0
   if { $pastrouve } {
      if { $trouvedef } {
         set astk::profil(version) $astk::config(-1,def_vers)
      } else {
         set astk::profil(version) $astk::ihm(lvers,0)
      }
   }
# aide contextuelle
   def_context $parent.vers 313
}

# appel� pour remettre � jour le nom du serveur, la liste des versions
# quand on s�lectionne un noeud diff�rent
# ihm="+ihm" pour mettre � jour l'ihm
#################################################################
proc change_noeud { } {
   set serv [quel_serveur $astk::profil(noeud)]
   if { $serv == "-" } {
      set astk::profil(serveur) $astk::config(-1,nom)
      return
   }
   set astk::profil(serveur) $astk::config($serv,nom)
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (change_noeud) $astk::profil(noeud) $serv $astk::profil(serveur)"
   }
   set oldbatch -1
   catch { set oldbatch $astk::profil(batch) }
   if { $oldbatch < 0 } {
   #  batch par d�faut si possible
      if { $astk::config($serv,batch) == "oui" } {
         set astk::profil(batch) 1
      } else {
         set astk::profil(batch) 0
      }
   }
   # maj de la liste des versions
   set astk::ihm(nb_vers) $astk::config($serv,nb_vers)
   set astk::ihm(lvers,0) ""
   for {set i 0} { $i < $astk::ihm(nb_vers) } {incr i} {
      set astk::ihm(lvers,$i) $astk::config($serv,vers,$i)
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (change_noeud) Version $i : $astk::ihm(lvers,$i)" }
   }
   mod_batch
   liste_versions $astk::ihm(satellite).subtit.version

   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (change_noeud) Nouveaux serveur/noeud/version : $astk::profil(serveur) $astk::profil(noeud) $astk::profil(version)"
   }
}

# retourne le label associ� � un onglet
#################################################################
proc quel_onglet { var } {
   set ONG ""
   if { $var == "sources" } {
      set var surcharge
   }
   for {set o 1} {$o <= $astk::ihm(nbongM)} {incr o} {
      if { $var == $astk::ihm(ong,$o,1) } {
         set ONG $astk::ihm(tit,$o,1)
         break
      }
   }
   return $ONG
}
