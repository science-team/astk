#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# SCRIPT PRINCIPAL : astk.tcl
# arguments : -debug [n] [-p nom_profil] + ...

# $Id: astk.tcl 2884 2007-03-20 17:00:36Z courtois $

# lancement depuis une autre appli (exemple depuis Salom�) :
# set argv "--from_salome"
# set argc [llength $argv]

# on cache la fen�tre en attendant la construction
wm withdraw .

set root [file dirname [info script]]
source [file join $root init_share.tcl]
source [file join $root init.tcl]

set ashare::appli "astk"
pre_traite_argv
init_gene $root
unset root

# pointeur off sert surtout � faire quelque chose sur "." pour qu'elle
# apparaisse avant la fenetre de log
ashare::pointeur off
ashare::init_log ".astk"

# initialisation des mots cl�s des diff�rents fichiers
init_mots

# initialisation des messages erreur/info/ihm
ashare::lire_msg [file join $ashare::root $ashare::fic_msg]

# pr�f�rences utilisateur
ashare::init_prefs
init_icon $astk::config(-1,theme)
set ashare::dbg $astk::config(-1,dbglevel)

# initialisation des noms d'onglet
def_onglet

set msg [ashare::mess "info" 1 $astk::astk_version]
puts $msg

# lecture des arguments
traite_argv

# initialisation du profil
init_profil

# initialisation des couleurs, fonts...
init_ihm

# verifications
check_conf $astk::check_quit

if { $ashare::dbg } {
   ashare::mess "info" 2 $ashare::dbg }

# splash screen
set splash ""
if { $ashare::splash_screen == 1 } {
   set splash .fen_about
   a_propos . [ashare::mess "ihm" 367]
}
if { $ashare::dbg > 0 } {
   show_fen $astk::ihm(log) force
}

# initialisation des ressources
init_env $splash

# initialisation agla
init_agla

# raffraichit donn�es+onglet agla
raffr_agla $splash INI

# onglet de demarrage
set astk::profil(onglet_actif) $astk::ihm(ong,1,1)

# initialisations finies
end_init

# fenetre principale d'ASTK
astk_princ

