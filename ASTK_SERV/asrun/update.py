# -*- coding: utf-8 -*-

# ==============================================================================
# COPYRIGHT (C) 1991 - 2003  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ==============================================================================

"""
Tools to maintain an astk installation.
Note that 'as_run' is a part of astk, so it tries to update itself.

Methods are called by an AsterRun object.
"""

import sys
import os
import re
from pprint import pformat
from distutils import sysconfig

from asrun.installation import aster_root
from asrun.common.i18n  import _
from asrun.mystring     import ufmt
from asrun.common.utils import version2tuple, tuple2version, sha1
from asrun.common_func  import get_tmpname
from asrun.system       import local_host


def AstkUpdate(run, *args):
    """Update current astk installation.
    usage: as_run --astk_update [--force] [--local] [version]

    download list of available version called "astk.VERSION" :
            version package_name sha1sum
    search the more recent version
    install it
    """
    if len(args) > 1:
        run.parser.error(_(u"'--%s' requires at most one argument") % run.current_action)

    ask = None
    if len(args) > 0:
        pkg = os.path.normpath(os.path.abspath(args[0]))
        if not os.path.exists(pkg):
            ask = args[0]

    if run['local']:
        REPSRC = run['local_rep_maj']
    else:
        REPSRC = run['http_server_user']+'@'+run['http_server_ip']+ \
                    ':'+run['http_rep_maj']

    run.print_timer = True
    run.PrintExitCode = False
    # ----- prepare temporary folder
    reptrav = get_tmpname(run, run['tmp_user'], basename='astk_update')
    run.ToDelete(reptrav)
    run.MkDir(reptrav)
    os.chdir(reptrav)

    # ----- download VERSION file
    tit = _(u'Download archives')
    run.timer.Start(tit)
    astkV = 'astk.VERSION'
    fvers = os.path.join(REPSRC, astkV)
    dest = os.path.join(reptrav, astkV)
    jret = run.Copy(dest, fvers, niverr='SILENT', protocol='HTTP')
    if jret != 0:
        run.Mess(ufmt(_(u"Can not download %s"), fvers), "<A>_FILE_NOT_FOUND")
        return
    lvers = read_version_list(dest, REPSRC)
    run.DBG(pformat(lvers), all=True)

    # ----- is there a new version ?
    vers = (0, 0, 0)
    current = version2tuple(run['version'])
    if len(lvers) > 0:
        vers, pkg, sha = lvers[0]
        run.Mess(_(u'Last release found : %s') % tuple2version(vers))

    # --vers= used ?
    if ask:
        ask = version2tuple(ask)
        nf = len(ask)
        vers = None
        for vers_i, pkg, sha in lvers:
            if vers_i[:nf] == ask:
                vers = vers_i
                break
        if vers is None:
            run.Mess(ufmt(_(u'version %s not found'), tuple2version(ask)), '<F>_ERROR')
    else:
        if vers <= current:
            # no update available
            run.Mess(_(u'no update found'), 'OK')
            return
    vnew = tuple2version(vers)

    # ----- hook for development
    hgrepo = os.path.join(sysconfig.get_python_lib(prefix=aster_root), 'asrun')
    cmd = 'cd %s ; hg id' % hgrepo
    ihg, out = run.Shell(cmd)
    if ihg == 0:
        run.Mess(_(u'Updating in a mercurial repository is not allowed!'))
        return

    # ----- download the package
    archive = os.path.join(reptrav, os.path.basename(pkg))
    jret = download_package(run, pkg, archive, sha)
    if jret == 4 or (jret == 2 and not run['force']):
        return
    run.timer.Stop(tit)

    # ----- installation pkg
    tit = _(u'Installation')
    run.timer.Start(tit)
    cmd = 'cd %(reptrav)s ; tar xzf %(archive)s ; cd %(content)s ; %(pythonexecutable)s setup.py install --prefix=%(aster_root)s' % {
        'aster_root'       : aster_root,
        'archive'          : archive,
        'content'          : re.sub('\.(tar\.gz|tgz)', '', os.path.basename(archive)),
        'pythonexecutable' : sys.executable,
        'reptrav'          : reptrav,
    }
    iret, out = run.Shell(cmd, follow_output=True)
    run.timer.Stop(tit)
    mail = []
    if iret == 0:
        res = 'COMPLETED'
        mail.append(_(u'astk/asrun update ended successfully from %s to %s') % (run['version'], vnew))
        run.Mess(os.linesep.join(mail), 'OK')
    else:
        res = 'FAILED'
        mail.append(_(u'astk/asrun update from %s to %s failed') % (run['version'], vnew))
        mail.extend(['Output :', '-'*60, out, '-'*60])
        run.Mess(os.linesep.join(mail), '<F>_UPDATE_FAILED')
    if run['report_to'] != '':
        run.SendMail(dest=run['report_to'],
            text = os.linesep.join(mail),
            subject = 'AstkUpdate %s on %s from %s to %s' % (res, local_host, run['version'], vnew))


def download_package(run, pkg, dest, shasum):
    """Download and check sum of package.
    """
    jret = run.Copy(dest, pkg, niverr='SILENT', protocol='HTTP')
    if jret != 0:
        run.Mess(ufmt(_(u"Can not download %s"), pkg), "<A>_FILE_NOT_FOUND")
        return 4
    shaD = sha1(open(dest, 'rb').read()).hexdigest()
    if shaD != shasum:
        run.Mess(_(u"SHA1 checksum failed (use --force to ignore this error) "), "<A>_CHECKSUM")
        run.Mess(_(u"       reference = %s") % shasum)
        run.Mess(_(u" downloaded file = %s") % shaD)
        return 2
    return 0


def read_version_list(filename, REPSRC):
    """Decode a file of list of packages.
    Format :
            version package_name sha1sum
            lines beginning by a # are ignored.
    """
    comm = re.compile('^#.*', re.MULTILINE)
    content = open(filename, 'r').read()
    content = comm.sub('', content)
    lvers = []
    for line in content.splitlines():
        fields = line.split()
        if len(fields) == 3:
            vers, pkg, sha = fields
            vers = version2tuple(vers)
            pkg  = os.path.join(REPSRC, pkg)
            lvers.append((vers, pkg, sha))
    lvers.sort(reverse=True)
    return lvers

