# AUTOMATICALLY GENERATED - DO NOT EDIT !
# Put all your changes in profile_local.csh in the same directory
#
# profile.csh : initialize the environment for as_run services
# (sh, ksh, bash syntax)
#
# If variables are depending on Code_Aster version, use ENV_SH in
# the corresponding 'config.txt' file.
#

if (!($?DEFINE_PROFILE_ASRUN) || !($?LD_LIBRARY_PATH)) then
   setenv DEFINE_PROFILE_ASRUN done
#--- ifndef DEFINE_PROFILE_ASRUN -----------------------------------------------

if !($?ASTER_ROOT) then
    # from first installation
    setenv ASTER_ROOT ?ASTER_ROOT?
endif
setenv ASTER_ETC $ASTER_ROOT/etc
if ( "X$ASTER_ROOT" == "X/usr" ) then
   setenv ASTER_ETC /etc
endif

if !($?PATH) then
   setenv PATH $ASTER_ROOT/bin:$ASTER_ROOT/outils
else
   setenv PATH $ASTER_ROOT/bin:$ASTER_ROOT/outils:$PATH
endif

if !($?LD_LIBRARY_PATH) then
   setenv LD_LIBRARY_PATH ?HOME_PYTHON?/lib
else
   setenv LD_LIBRARY_PATH ?HOME_PYTHON?/lib:$LD_LIBRARY_PATH
endif

if !($?PYTHONPATH) then
   setenv PYTHONPATH ?ASRUN_SITE_PKG?
else
   setenv PYTHONPATH ?ASRUN_SITE_PKG?:$PYTHONPATH
endif

setenv PYTHONEXECUTABLE ?PYTHON_EXE?

# this should not be usefull...
#setenv PYTHONHOME ?HOME_PYTHON?

setenv WISHEXECUTABLE ?WISH_EXE?


# source local profile
if (-e $ASTER_ETC/codeaster/profile_local.csh) then
   source $ASTER_ETC/codeaster/profile_local.csh
endif

#--- endif DEFINE_PROFILE_ASRUN ------------------------------------------------
endif

