#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import os.path as osp
import re
import unittest
from pprint import pformat

from common import dict_conf, tmpdir
from data import agla_export

import asrun
from asrun.core         import magic
from asrun.profil import AsterProfil
from asrun.run    import AsRunFactory
from asrun.build  import AsterBuild
from asrun.config import AsterConfig
from asrun.mystring import print3


funig_old = osp.join(tmpdir, "agla.old_unig")
funig_new = osp.join(tmpdir, "agla.new_unig")


class TestAgla(unittest.TestCase):

    def test01_profil(self):
        prof = AsterProfil()
        prof.parse(agla_export % dict_conf)
        filename = osp.join(tmpdir, "agla.export")
        prof.WriteExportTo(filename)
        assert osp.exists(filename)
        prof_cnt = prof.get_content()
        assert prof_cnt.find('N py fichier_inexistant D 0') > -1
        assert len(prof.agla) == 3
        names = [val[2] for val in prof.agla]
        assert "macr_lign_coupe" in names


    def test02_unigest(self):
        run = AsRunFactory()
        magic.set_stdout(osp.join(tmpdir, "agla.2.out"))
        fconf = osp.join(run.get_version_path(dict_conf["ASTER_VERSION"]), "config.txt")

        conf = AsterConfig(fconf, run)
        build = AsterBuild(run, conf)

        # old syntax
        open(funig_old, 'w').write("""
FORSUPPR zzzzzz repf77
F90SUPPR yyyyyy repf90
CSUPPR cccccc repc
PYSUPPR pppppp reppy
CATSUPPR aaaaaa typelem
CATSUPPR bbbbbb commande
TESSUPPR tttt123a
TESSUPPR tttt123a.22 meunier S.MEUNIER
TESSUPPR tttt123a.37 meunier S.MEUNIER
""")

        # new syntax
        open(funig_new, 'w').write("""
SUPPR bibfor/repf77/zzzzzz.f
SUPPR bibf90/repf90/yyyyyy.F
SUPPR bibc/repc/cccccc.c
SUPPR bibpyt/reppy/pppppp.py
SUPPR catalo/typelem/aaaaaa.cata
SUPPR catapy/commande/bbbbbb.capy
SUPPR astest/tttt123a.comm
SUPPR astest/tttt123a.22
SUPPR astest/tttt123a.37
""")
        dold = build.GetUnigest(funig_old)
        dnew = build.GetUnigest(funig_new)
        print3(pformat(dold))
        print3(pformat(dnew))
        diff_keys = set(dold.keys()).symmetric_difference(dnew.keys())
        assert len(diff_keys) == 0

        for k, lold in dold.items():
            if not type(lold) in (list, tuple):
                continue
            while len(lold) > 0:
                val = lold.pop()
                assert val in dnew[k], "%s not in %s" % (val, dnew[k])
                dnew[k].remove(val)

        for k, lnew in dnew.items():
            if not type(lnew) in (list, tuple):
                continue
            assert len(lnew) == 0, 'dnew[%s] should be empty !' % k

        os.remove(funig_old)
        os.remove(funig_new)


if __name__ == "__main__":
    unittest.main()

