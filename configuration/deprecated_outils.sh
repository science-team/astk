#!/bin/bash
# command move from ../outils to ../bin
set_prefix() {
   old=`readlink -n -f $1`
   local bin=`dirname $old`
   prefix=`dirname $bin`
}
set_prefix $0

new_command=$prefix/bin/`basename $old`
echo "DeprecationWarning: "$old" moved to "$new_command". Please update your PATH. It will be an error in a future release."
$new_command $*

