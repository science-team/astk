#!/bin/bash
# command move from ../ASTK/ASTK_yyy/bin/xxx to ../bin/xxx
set_prefix() {
   old=`readlink -n -f $1`
   local bin=`dirname $old`
   local astk_yyy=`dirname $bin`
   local astk=`dirname $astk_yyy`
   prefix=`dirname $astk`
}
set_prefix $0

new_command=$prefix/bin/`basename $old`
echo "DeprecationWarning: "$old" renamed to "$new_command". Please update your script. It will be an error in a future release."
$new_command $*

