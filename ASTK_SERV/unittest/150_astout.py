#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as osp
import unittest

from common import dict_conf, execcmd, tmpdir, aster_version
from data   import astout_export

import asrun


class TestAstout(unittest.TestCase):

    def test01_cmdline(self):
        from asrun.run import AsRunFactory
        run = AsRunFactory()
        srcfiles = osp.join(run.get_version_path(aster_version), "astest", "efica02a.*")
        export = osp.join(tmpdir, "astout.export")
        #dict_conf['ASTER_VERSION'] = 'NEW10_intel'
        open(export, "w").write(astout_export % dict_conf)
        dest = osp.join(tmpdir, "astout.rep_test")
        try:
            os.makedirs(dest)
        except OSError:
            pass
        iret = os.system("cp %s %s" % (srcfiles, dest))
        cmd = dict_conf["as_run"] + ["--only_nook", export]
        iret = execcmd(cmd, "astout.1")
        assert iret == 0


if __name__ == "__main__":
    unittest.main()

