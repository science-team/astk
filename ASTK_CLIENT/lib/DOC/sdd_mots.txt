# liste des mots cl�s des fichiers de config + options
# fichier serveur
   astk::ihm(MCS_serv)
# fichier conf
   astk::ihm(MCS_conf)
   astk::ihm(MCF_conf)
   astk::ihm(SSF_conf)
# fichier hosts
   astk::ihm(MCF_hosts)
   astk::ihm(SSF_hosts)
# fichier prefs
   astk::ihm(MCS_pref)
   astk::ihm(MCS_pref_ign)
# fichier agla
   astk::ihm(MCS_agla)
   astk::ihm(MCF_agla)
   astk::ihm(SSF_agla)
# fichier limit
   astk::ihm(MCS_limit)
# fichier des derniers profils
   astk::ihm(MCS_prof)

