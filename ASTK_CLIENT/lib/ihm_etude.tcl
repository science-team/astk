#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_etude.tcl 653 2004-09-24 14:44:45Z mcourtoi $

# Onglet ETUDE
#################################################################
proc affiche_etude {} {
   pack [frame $astk::ihm(fenetre).active -relief raised -bd 1] -anchor nw -fill both -expand 1

# frame bouton et chemin de base
   grid [frame $astk::ihm(fenetre).active.base -relief raised -bd 1] -row 0 -column 0 -sticky new -padx 3 -pady 3
      fenEEC2 $astk::ihm(fenetre).active.base [ashare::mess ihm 38]
      pack [frame $astk::ihm(fenetre).active.base.basepath -bd 0] -padx 3 -pady 0 -fill x

# frame liste des fichiers et icones
	grid [frame $astk::ihm(fenetre).active.princ -relief raised -bd 1] -row 1 -column 0 -sticky nsew -padx 3 -pady 3

      set sw [ScrolledWindow $astk::ihm(fenetre).active.princ.lst]
      set sf [ScrollableFrame $sw.f]
      $sw setwidget $sf
      set liste [$sf getframe]
      $sf configure -constrainedwidth 1
      # liste = ....active.princ.lst.f.frame
      grid $sw -row 0 -column 0 -sticky nsew
      grid [frame $astk::ihm(fenetre).active.princ.icone] -row 0 -column 1 -sticky nw
      grid rowconfigure    $astk::ihm(fenetre).active.princ 0 -weight 1
      grid columnconfigure $astk::ihm(fenetre).active.princ 0 -weight 1

# frame argument
	grid [frame $astk::ihm(fenetre).active.arg -relief raised -bd 1] -row 2 -column 0 -sticky sew -padx 3 -pady 3

   grid rowconfigure    $astk::ihm(fenetre).active 1 -weight 1
   grid columnconfigure $astk::ihm(fenetre).active 0 -weight 1

   chemin_base $astk::ihm(fenetre).active.base.basepath $liste etude
   liste_fich  $liste -1 etude
   liste_icone $astk::ihm(fenetre).active.princ.icone $liste etude

	affiche_arg $astk::ihm(fenetre).active.arg
}

# Onglet TESTS
#################################################################
proc affiche_tests {} {
   pack [frame $astk::ihm(fenetre).active -relief raised -bd 1] -anchor nw -fill both -expand 1

# frame bouton et chemin de base
   grid [frame $astk::ihm(fenetre).active.base -relief raised -bd 1] -row 0 -column 0 -sticky new -padx 3 -pady 3
      fenEEC2 $astk::ihm(fenetre).active.base [ashare::mess ihm 39]
      pack [frame $astk::ihm(fenetre).active.base.basepath -bd 0] -padx 3 -pady 0 -fill x

# frame liste des fichiers et icones
	grid [frame $astk::ihm(fenetre).active.princ -relief raised -bd 1] -row 1 -column 0 -sticky nsew -padx 3 -pady 3

      set sw [ScrolledWindow $astk::ihm(fenetre).active.princ.lst]
      set sf [ScrollableFrame $sw.f]
      $sw setwidget $sf
      set liste [$sf getframe]
      $sf configure -constrainedwidth 1
      # liste = ....active.princ.lst.f.frame
      grid $sw -row 0 -column 0 -sticky nsew
      grid [frame $astk::ihm(fenetre).active.princ.icone] -row 0 -column 1 -sticky nw
      grid rowconfigure    $astk::ihm(fenetre).active.princ 0 -weight 1
      grid columnconfigure $astk::ihm(fenetre).active.princ 0 -weight 1

# frame argument
	grid [frame $astk::ihm(fenetre).active.arg -relief raised -bd 1] -row 2 -column 0 -sticky sew -padx 3 -pady 3

   grid rowconfigure    $astk::ihm(fenetre).active 1 -weight 1
   grid columnconfigure $astk::ihm(fenetre).active 0 -weight 1

   chemin_base $astk::ihm(fenetre).active.base.basepath $liste tests
   liste_fich  $liste -1 tests
   liste_icone $astk::ihm(fenetre).active.princ.icone $liste tests

	affiche_arg $astk::ihm(fenetre).active.arg
}
