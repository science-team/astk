#!/usr/bin/make -f
# Copyright 2008 - Sylvestre Ledru <sylvestre.ledru@inria.fr>

ASTER_VERSION=11.4.0
ASTER_VERSION_FULL=$(ASTER_VERSION)-1
ASTER_ROOT=/usr/lib/codeaster

DEB_SOURCE_PACKAGE := astk
DEB_UPSTREAM_VERSION := 1.13.1

BUILDDIR = $(CURDIR)/debian/build
DEB_DESTDIR = $(CURDIR)/debian/tmp

DEB_HOST_ARCH_OS=$(shell dpkg-architecture -qDEB_HOST_ARCH_OS | tr '[:lower:]' '[:upper:]')
DEB_HOST_ARCH_BITS=$(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)


%:
	dh $@  --builddirectory=$(BUILDDIR) --with python2

DEB_PYTHON_COMPILE_VERSION := $(shell pyversions -r)
DEB_PYTHON_INSTALL_ARGS_ALL += --no-compile \
	--install-lib=usr/share/pyshared --install-layout=deb

export DESTDIR = $(DEB_DESTDIR)

override_dh_auto_install:
	dh_auto_install -- 
#install/code-gui-run:: 
	rm -f $(DEB_DESTDIR)/usr/lib/astk/BWidget-1.7.0/LICENSE.txt
	dh_link usr/bin/codeaster-client usr/bin/codeaster-gui

	# Add links in $ASTER_ROOT/outils
	mkdir -p debian/code-aster-gui/$(ASTER_ROOT)/outils
	ln -sf /usr/bin/eficas debian/code-aster-gui/$(ASTER_ROOT)/outils
	ln -sf /usr/bin/eficasQt debian/code-aster-gui/$(ASTER_ROOT)/outils
	ln -sf /usr/bin/gmsh debian/code-aster-gui/$(ASTER_ROOT)/outils
	ln -sf /usr/bin/xmgrace debian/code-aster-gui/$(ASTER_ROOT)/outils

#install/code-aster-run:: 
# install update-codeaster-engine script
	mkdir -p $(DEB_DESTDIR)/usr/sbin
	cp debian/update-codeaster-engines $(DEB_DESTDIR)/usr/sbin
	chmod 755 $(DEB_DESTDIR)/usr/sbin/update-codeaster-engines

	# Fix platform in ASTK_SERV/etc/asrun (/etc/codeaster/asrun)
	if [ "${DEB_HOST_ARCH_BITS}" = "64" ]; then \
	   perl -pi -e "s|\?IFDEF\?|$(DEB_HOST_ARCH_OS)$(DEB_HOST_ARCH_BITS)|" ASTK_SERV/etc/asrun; \
	else \
	   perl -pi -e "s|\?IFDEF\?|$(DEB_HOST_ARCH_OS)|" ASTK_SERV/etc/asrun; \
	fi
	
	#fix lintian error
	#chmod  ugo-x $(DEB_DESTDIR)debian/code-aster-run/$(ASTER_ROOT)/asrun/unittest/run_test.py


# how to remove duplicate .py in $(ASTER_ROOT)/asrun/unittest??

	mkdir -p debian/tmp/usr/lib/codeaster/bin
	ln -sf /usr/bin/as_run debian/tmp/usr/lib/codeaster/bin/as_run

	cd $(DEB_DESTDIR)/usr/bin && mv get codeaster-get
	cd $(DEB_DESTDIR)/usr/bin && mv parallel_cp codeaster-parallel_cp
	cd $(DEB_DESTDIR)/usr/bin && ln -sf as_run codeaster

override_dh_auto_clean:
	rm -rf $(BUILDDIR)
	rm -rf $(CURDIR)/debian/tmp
	find . -name \*.pyc | xargs -r rm
	debconf-updatepo

get-orig-source:
	# Retreive aster-full-src-$(ASTER_VERSION_FULL).noarch.tar.gz
	mkdir -p tmp
	cd tmp && wget "http://www.code-aster.org/FICHIERS/aster-full-src-$(ASTER_VERSION_FULL).noarch.tar.gz"

	# Go to SRC directory and retreive astk source
	cd tmp && tar xzf aster-full-src-$(ASTER_VERSION_FULL).noarch.tar.gz
	cd tmp/aster-full-src-$(ASTER_VERSION)/SRC && mv $(DEB_SOURCE_PACKAGE)-$(DEB_UPSTREAM_VERSION).tar.gz $(CURDIR)/$(DEB_SOURCE_PACKAGE)_$(DEB_UPSTREAM_VERSION).orig.tar.gz
	rm -rf tmp
