#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_salome.tcl 2890 2007-04-03 09:40:33Z courtois $

# namespace partag�
#################################################################
namespace eval salome_vars {
   variable l_filename
}

# action nouveau appel�e uniquement depuis Salom�
#################################################################
proc salome_nouv_act { fenetre_liste var nserv path type ul donnee resultat compress} {
   set ideb 0
   modprof
   set i $astk::profil($var,nbfic)
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (salome_nouv_act) $fenetre_liste $var => astk::profil($var,fich,$i,XXX)"
   }
   set astk::profil($var,fich,$i,nom)      $path
   set astk::profil($var,fich,$i,serv)     $astk::config($nserv,nom)
   set astk::profil($var,fich,$i,type)     $type
   set astk::profil($var,fich,$i,FR)       "F"
   set astk::profil($var,fich,$i,UL)       0
   incr astk::profil($var,nbfic)
   liste_fich $fenetre_liste [expr $astk::profil($var,nbfic) - 1] $var
   for {set nul 0} {$nul < $astk::UL_ref($var,nbre)} {incr nul} {
      if { $astk::profil($var,fich,$i,type) == $astk::UL_ref($var,$nul,nom) } {
         break
      }
   }
   if { $nul >= $astk::UL_ref($var,nbre)} {
      set nul 0
      set astk::profil($var,fich,$i,type) $astk::UL_ref($var,$nul,nom)
   }
   set_UL $i $nul $var
   if { $ul > 0 && $var == "etude" } {
        set astk::profil($var,fich,$i,UL) $ul
   }
   set astk::profil($var,fich,$i,donnee)   $donnee
   set astk::profil($var,fich,$i,resultat) $resultat
   set astk::profil($var,fich,$i,compress) $compress
}


# action export vers Salome
#  on exporte les fichiers rmed du profil
#  ou le fichier s�lectionn� qq soit son type si 'from_sel'
#################################################################
proc salome_export_act { {from_sel profil} } {
   set salome_vars::l_filename {}
   set lfich {}
   if { $from_sel == "from_sel" } {
      set lfsel $astk::sel(filename)
      for {set i 0} {$i < [llength $lfsel]} {incr i} {
         set serv $astk::inv(serv,$astk::sel(servname))
         set nom [lindex $lfsel $i]
         set var $astk::sel(liste)
         set ind $astk::sel(indice)
         set typ $astk::profil($var,fich,$ind,type)
         set ul  $astk::profil($var,fich,$ind,UL)
         lappend lfich [list $serv $nom $typ $ul]
      }
   } else {
      set var $astk::profil(onglet_actif)
      for {set i 0} {$i < $astk::profil($var,nbfic)} {incr i} {
         if { $astk::profil($var,fich,$i,type) == "rmed" } {
            set tmp $astk::profil($var,fich,$i,nom)
            if { [string index $tmp 0] != "/" } {
               set tmp [file join $astk::profil(path_$var) $tmp]
            }
            set serv $astk::inv(serv,$astk::profil($var,fich,$i,serv))
            set typ $astk::profil($var,fich,$i,type)
            set ul  $astk::profil($var,fich,$i,UL)
            lappend lfich [list $serv $tmp $typ $ul]
         }
      }
   }
   set salome_vars::l_filename $lfich
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (salome_export_event) Contenu de salome_vars:: (longueur [llength $salome_vars::l_filename])"
      for {set i 0} {$i < [llength $salome_vars::l_filename]} {incr i} {
         ashare::log "        $i fich=[lindex $salome_vars::l_filename $i]"
      }
   }
   # g�n�re l'�v�nement SalomeExport
   if { [llength $salome_vars::l_filename] > 0 } {
      event generate . <<SalomeExport>>
   } else {
      set msg [ashare::mess ihm 25]
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      change_status $msg
   }
}
