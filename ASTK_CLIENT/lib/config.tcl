#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: config.tcl 3060 2007-07-05 07:34:54Z courtois $

# mots-cl�s du fichier de config
#################################################################
proc ashare::init_mots_config { } {
# MCS : Mots-Cl�s Simples
# MCF : Mot-Cl� Facteur (un seul)
# SSF : mots-cl�s Simples Sous un mot-cl� Facteur
# fichier de configuration des serveurs
   set ashare::mots(MCF_serv) { serveur }
   set ashare::mots(MCS_serv) { nom_complet login home etat rep_serv asrun_vers recup last_recup \
                                xterm editeur }
   set ashare::mots(MCS_serv_opt) { plate-forme
                             batch batch_tpsmax batch_memmax batch_nbpmax batch_mpi_nbpmax batch_queue_group
                             interactif interactif_tpsmax interactif_memmax interactif_nbpmax interactif_mpi_nbpmax
                             vers noeud mach_ref islocal
                             protocol_exec protocol_copyto protocol_copyfrom proxy_dir
                             schema_actu
                             schema_edit
                             schema_get_results
                             schema_info
                             schema_purge_flash
                             schema_sendmail
                             schema_serv
                             schema_stop_del
                             schema_tail
                             schema_get_export
                             schema_create_issue
                             schema_insert_in_db
                             schema_profile_modifier
                             schema_calcul
                             schema_execute
                             }
}

# Lecture de la liste des serveurs disponibles et de leurs options
#################################################################
proc ashare::lire_options { fich tabl } {
   upvar $tabl config
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_options) Lecture $fich" }

   set config(nb_serv) 0

   set iret [lire_mc_val $fich mots vale nlu ]
   if { $iret != 0 } {
      return $iret
   }

   # phase de v�rif
   # mots-cl�s obligatoires
   set mots_smcf $ashare::mots(MCS_serv)
   # mots-cl�s facteurs
   set mots_cles $ashare::mots(MCF_serv)
   set pmcs [llength $mots_smcf]
   set pmcf [llength $mots_cles]
   # mots-cl�s optionnels
   append mots_smcf $ashare::mots(MCS_serv_opt)
   append mots_cles $mots_smcf
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set pres($mcs) 0
   }

   set nbmcf -1
   set nbaux 0
   set nberr 0
   for {set j 1} {$j <= $nlu} {incr j} {
      #astkrc_version
      if { $mots($j) == "astkrc_version" } {
         if { $vale($j) != "$ashare::astkrc_version" } {
            ashare::mess "erreur" 4 $fich
            return PB_astkrc_version
         }
      } else {
         #ashare::log "$j $mots($j) $vale($j)"
         set iv [lsearch -exact $mots_cles $mots($j)]
         if { $iv < 0 } {
            ashare::mess "erreur" 5 $mots($j)
            incr nberr
         } else {
            incr pres($mots($j))
            set i [lsearch -exact $mots_smcf $mots($j)]
            if { $i < 0 } {
               if { $nbmcf >= 0 && $config($nbmcf,etat) == "off" } {
                  set config($nbmcf,mach_ref) "non"
               }
               incr nbmcf
               set config($nbmcf,nom) $vale($j)
            # valeurs par d�faut pour un serveur
               set config($nbmcf,islocal)  "non"
               set config($nbmcf,nb_vers)  0
               set config($nbmcf,nb_noeud) 0
               set config($nbmcf,asrun_vers) "00.00.00"
               for { set im 0 } { $im < [ llength $ashare::mots(MCS_serv_opt) ] } { incr im } {
                  set mcs [lindex $ashare::mots(MCS_serv_opt) $im]
                  if { $mcs != "vers" & $mcs != "noeud" } {
                     set config($nbmcf,$mcs) ""
                  }
               }
            } else {
               #ashare::log "Pour le serveur $nbmcf :"
               if { $nbmcf >= 0 } {
                  if { $mots($j) == "vers" || $mots($j) == "noeud" } {
                     set config($nbmcf,$mots($j),$config($nbmcf,nb_$mots($j))) $vale($j)
                     incr config($nbmcf,nb_$mots($j))
                     incr nbaux
                  } elseif { $mots($j) == "login" && $vale($j) == "username" } {
                     set config($nbmcf,$mots($j)) [exec whoami]
                  } else {
                     set config($nbmcf,$mots($j)) $vale($j)
                  }
               } else {
                  ashare::mess "erreur" 6 "Serveur" "Server" $mots($j) ""
                  incr nberr
               }
            }
         }
      }
   }
   if { $nbmcf < 0 } {
      ashare::mess "erreur" 7 "serveur" "server" ""
#      incr nberr  pas fatal car on a initialis� nb_serv=0
   }
   incr nbmcf
   set config(nb_serv) $nbmcf
   # est-ce qu'il manque un mot-cl� ?
   set sum 0
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set sum [expr $sum + $pres($mcs)]
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_options) $nbmcf serveurs"
   }
   if { $nberr > 0 } {
      ashare::mess "erreur" 8 $fich
      ashare::my_exit 4
   }
   return 0
}

# Sauvegarde des options (serveurs et options de config)
#################################################################
proc ashare::save_options { } {
   # garde l'ancien fichier
   set old "$astk::fic_serveurs.old"
   if { [file exists $astk::fic_serveurs ] } {
      file rename -force $astk::fic_serveurs $old
   }
   # sauve la configuration des serveurs
   set id [open $astk::fic_serveurs w]
   puts $id "# AUTOMATICALLY GENERATED - DO NOT EDIT !"
   puts $id "astkrc_version : $ashare::astkrc_version"
   puts $id "#"
   set lMCS $ashare::mots(MCS_serv)
   append lMCS $ashare::mots(MCS_serv_opt)
   for { set serv 0 } { $serv < $astk::config(nb_serv) } { incr serv } {
      puts $id "serveur : $astk::config($serv,nom)"
      # sauve la config de chaque serveur
      for { set im 0 } { $im < [ llength $lMCS ] } { incr im } {
         set mcs [lindex $lMCS $im]
         if { $mcs == "login" && $astk::config($serv,$mcs) == "" } {
            set astk::config($serv,$mcs) $astk::config(-1,$mcs)
         }
         if { $mcs == "vers" || $mcs == "noeud" } {
            for { set j 0 } { $j < $astk::config($serv,nb_$mcs) } { incr j } {
               set val [unexpandvars $astk::config($serv,$mcs,$j)]
               puts $id "$mcs : $val"
            }
         } else {
            set val  $astk::config($serv,$mcs)
            if { $val == "" } {
               set val "_VIDE"
            }
            set val [unexpandvars $val]
            puts $id "$mcs : $val"
         }
      }
      puts $id "#"
   }
   close $id
   ashare::mess "info" 14 [file tail $old]
   return 0
}

# Retourne le nom du user (from tkdiff)
#################################################################
proc whoami {} {
    global env
    if {[info exists env(USER)]} {
        return $env(USER)
    }
    if {[info exists env(LOGNAME)]} {
        return $env(LOGNAME)
    }
    if {[info exists env(USERNAME)]} {
        return $env(USERNAME)
    }
    if {[info exists env(VCSID)]} {
        return $env(VCSID)
    }
    if {[catch {exec whoami} whoami]} {
        return nobody
    }
    return $whoami
}
