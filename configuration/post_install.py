# -*- coding: utf-8 -*-

import sys
import os
import os.path as osp
import re
from glob import glob
from distutils           import log, sysconfig
from distutils.file_util import copy_file, move_file
from distutils.dir_util  import mkpath, copy_tree

from configuration.util  import (read_rcfile, replace_rcfile, print_diff, backup_name,
                                 get_function, chmod_scripts, remove_tree, prefix2etc,
                                 less_than_version)


# To change files, I thought of extending distutils.build_scripts - which works
# only on python script - but I need --prefix= value which is not defined
# at build step (not sure) and "data_files" are not in 'build' directory.

# regular expression for variables of this form : ?VVVVVV?
VAR_REGEXP = re.compile('\?{1}([^\'\"\n|]+?)\?{1}', re.MULTILINE)
# must not be changed
EXCEPTIONS = ('vers : VVV', )


def finalize(rootdir, prefix, parameters, version, **options):
   """Finalize the installation :
   - configure files using 'parameters' dict,
   - add some DeprecatingWarning
   """
   log.info("finalizing installation in %s...", prefix)
   # fill the variables of the form ?VVV?
   files = get_files_to_change(rootdir, prefix)
   for filename in files:
      log.info("configuring %s...", filename)
      change_file(filename, parameters)
   # already done by install_scripts but just changed...
   chmod_scripts(glob(os.path.join(rootdir + prefix, 'bin', '*')))
   shortcuts = ['codeaster-run', ]
   if options.get("asrun_shortcuts"):
       shortcuts.extend(['show', 'get', 'showop'])
   for dest, l_alias in [('as_run', shortcuts),
                         ('astk', ('as_client', 'codeaster-client'))]:
      for alias in l_alias:
          alias = os.path.join(rootdir + prefix, 'bin', alias)
          try:
             if os.path.exists(alias):
                os.remove(alias)
             os.symlink(dest, alias)
          except (IOError, OSError):
             pass

   files = set(files)
   # fill configuration files (restore fields of the form 'parameter : value')
   if parameters['server_confdir'] != '':
      for new, previous in parameters['server_config_files'].items():
         param_file = parameters['server_conf_values'][new]
         new = os.path.join(prefix2etc(prefix), 'codeaster', new)
         files.add(new)
         # insert previous values
         log.info("restoring values from %s to %s", previous, new)
         replace_rcfile(new, param_file)
         if os.path.exists(previous):
            copy_file(previous, backup_name(new, '.orig'))
##            log.info(print_diff(backup_name(new, '.orig'), new))  # python-Bugs-1541642
      # copy previously existing files
      newdir = osp.join(prefix2etc(prefix), 'codeaster')
      # do not overwrite changed files
      ignore_list = files.copy()
      ignore_list.update(parameters['server_config_files'].values())
      # exceptions
      ignore_list.add(osp.join(parameters['server_confdir'], 'aster_profile.sh'))
      for old_file in os.listdir(parameters['server_confdir']):
         old = osp.join(parameters['server_confdir'], old_file)
         new = osp.join(newdir, old_file)
         if new in ignore_list or old in ignore_list:
            continue
         if not osp.exists(new):
            if osp.isfile(old):
                copy_file(old, new)
            else:
                os.makedirs(new)
                copy_tree(old, new)
   # restore auto_update.cron values
   dico = parameters['server_conf_values'].get('auto_update.cron')
   if dico:
       fname = os.path.join(rootdir + prefix, 'bin', 'auto_update.cron')
       dreg = {}
       for var, value in dico.items():
           dreg['%s=.*' % re.escape(var)] = '%s=%s' % (var, value)
       replace_from_expr(fname, {}, dreg)

   # restore client configuration file
   if parameters['client_confdir'] != '':
      for new, previous in parameters['client_config_files'].items():
         new = osp.join(prefix2etc(prefix), 'codeaster', new)
         if os.path.exists(previous):
            move_file(new, backup_name(new, '.new'))
            copy_file(previous, new)

   # configuration
   if version:
      migrate(prefix, parameters, version)

   # keep deprecation warning up to 1.9.0
   if not options.get("nodeprecation") and \
      (version is None or less_than_version(version, '1.9.0')):
      backward_compatibility(rootdir + prefix)

   # check for residual variables
   files = list(files)
   files.sort()
   for filename in files:
      check_unchanged_residual(filename)

   remove_tree(os.path.join(rootdir + prefix, '.as_run_configuration'))
   log.info("post-installation completed")


def migrate(prefix, param, version):
   """Migration of config files."""
   log.info("migrating from %s...", version)
   # server
   if param['client_confdir']:
      prefs = param['client_config_files'][os.path.join('astkrc', 'prefs')]
      if os.path.exists(prefs):
         filename = os.path.join(prefix2etc(prefix), 'codeaster', 'asrun')
         d = {}
         read_rcfile(prefs, d)
         terminal = d.get('xterm')
         if terminal:
            log.info("configuring TERMINAL in %s...", filename)
            change_file(filename, { 'TERMINAL' : terminal })

   # client : rep_serv in config_serveurs
   filename = os.path.join(prefix2etc(prefix), 'codeaster', 'astkrc', 'config_serveurs')
   dict_sub = ( ('0.0.0', { '^rep_serv *: *(.*)/ASTK/ASTK_SERV/bin' : r'rep_serv : \1' }),
                ('1.7.99', None),)
   dexpr = get_function(dict_sub, version)
   log.info("configuring %s...", filename)
   replace_from_expr(filename, param, dexpr)


def replace_from_expr(filename, param, dict_expr):
   """In 'filename' replace regexp matchings using 'param'."""
   if not dict_expr or not os.path.isfile(filename):
      return
   content = open(filename, 'r').read()
   for expr, repl in dict_expr.items():
      regexp = re.compile(expr, re.MULTILINE)
      content = regexp.sub(repl % param, content)
   open(filename, 'w').write(content)


def change_file(filename, dict_trans, backup_extension=''):
   """Change the variables in 'filename' (according to VAR_REGEXP)"""
   def func_repl(matchobj):
      var = matchobj.group(1)
      val = dict_trans.get(var, '?' + var + '?')
      return str(val)

   if not os.path.isfile(filename):
      log.warn("warning: file not found: %s", filename)
      return
   if not os.access(filename, os.W_OK):
       log.warn("no sufficient permission to change '%s', skipped.", filename)
       return

   content = open(filename, 'r').read()
   if backup_extension:
      open(filename + backup_extension, 'w').write(content)
   changed = VAR_REGEXP.sub(func_repl, content)
   open(filename, 'w').write(changed)


def check_unchanged_residual(filename):
   """Check for undefined variables.
   """
   content = open(filename, 'r').read()
   unchanged = set(VAR_REGEXP.findall(content))
   unchanged.difference_update(EXCEPTIONS)
   if len(unchanged) > 0:
      unchanged = list(unchanged)
      unchanged.sort()
      log.info("these variables have not been defined in %s", filename)
      log.info('  ' + ', '.join(unchanged))


def get_files_to_change(rootdir, prefix):
   """Build the list of files to change."""
   log.info("building list of the files to change (from %s)...", prefix)
   log.debug("... searching from %s", rootdir + prefix)
   log.debug("... searching from %s", rootdir + prefix2etc(prefix))
   # glob ignores files beginning with a dot (.mysql_connect)
   files = []
   for f in ('as_run*', 'astk', 'bsf', 'codeaster*', 'parallel_cp', 'auto_update.cron'):
      files.extend(glob(os.path.join(rootdir + prefix, 'bin', f)))
   files.extend(glob(os.path.join(rootdir + prefix2etc(prefix), 'codeaster', '*')))
   files.extend(glob(os.path.join(rootdir + prefix2etc(prefix), 'codeaster', 'astkrc', '*')))
   files.extend(glob(os.path.join(rootdir + prefix, 'share', 'codeaster', 'asrun', 'data', '*')))
   files.extend([os.path.join(sysconfig.get_python_lib(prefix=rootdir+prefix), 'asrun', 'installation.py')])
   files = [f for f in files if os.path.isfile(f)]
   log.info(" %8d files found", len(files))
   log.debug("%s", repr(files))
   return files


def backward_compatibility(prefix):
   # add DeprecationWarning
   scripts = []
   deprecated_sh = os.path.join(prefix, '.as_run_configuration', 'deprecated_outils.sh')
   mkpath(os.path.join(prefix, 'outils'))
   for prg in ('as_run', 'astk', 'bsf', 'show', 'get', 'getop'):
      file = os.path.join(prefix, 'outils', prg)
      log.info("deprecating %s", file)
      copy_file(deprecated_sh, file)
      scripts.append(file)

   deprecated_sh = os.path.join(prefix, '.as_run_configuration', 'deprecated_server.sh')
   mkpath(os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'bin'))
   for prg in ('as_run', 'as_serv'):
      file = os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'bin', prg)
      log.info("deprecating %s", file)
      copy_file(deprecated_sh, file)
      scripts.append(file)

   deprecated_lib = glob(os.path.join(prefix, '.as_run_configuration', 'astk_serv_lib', '*'))
   mkpath(os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'lib'))
   for file in deprecated_lib:
      prg = os.path.join(prefix, 'ASTK', 'ASTK_SERV', 'lib', os.path.basename(file))
      log.info("deprecating %s", prg)
      copy_file(file, prg)

   deprecated_sh = os.path.join(prefix, '.as_run_configuration', 'deprecated_client.sh')
   mkpath(os.path.join(prefix, 'ASTK', 'ASTK_CLIENT', 'bin'))
   for prg in ('astk', 'bsf'):
      file = os.path.join(prefix, 'ASTK', 'ASTK_CLIENT', 'bin', prg)
      log.info("deprecating %s", file)
      copy_file(deprecated_sh, file)
      scripts.append(file)

   chmod_scripts(scripts)

